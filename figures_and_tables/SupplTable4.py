from __future__ import absolute_import, division, print_function
import os
import pandas as pd
from idtrackerai.utils.GUI_utils import selectDir


def f_string(x):
    return x


def f_percentage(x):
    return '%.2f' % (x*100) if x != 1 else '%i' % (x*100)


def f_accuracy(x):
    return '%.3f' % (x*100)


def f_individual_accuracy(x):
    return '%.3f' % (x*100) if x != 1 else '%i' % (x*100)


def f_time(x):
    return '%.2f' % x


def f_area(x):
    return '%i' % x


def f_float(x):
    return '%.2f' % x


def f_integer(x):
    return '%i' % x


def f_boolean(x):
    return 'yes' if x else 'no'


def write_latex_table_for_subset_dataframe(tracked_videos_folder, data_frame,
                                           columns_to_include,
                                           new_columns_names, formatters,
                                           subset_condition, subtable_name):
    assert len(columns_to_include) == len(new_columns_names) == len(formatters)
    subset_data_frame = data_frame[subset_condition]
    subset_data_frame = subset_data_frame[columns_to_include].copy()
    subset_data_frame.columns = new_columns_names
    latex_table_name = subtable_name + '.tex'
    latex_table_path = os.path.join(tracked_videos_folder, latex_table_name)
    with open(latex_table_path, 'w') as file:
        file.write(subset_data_frame.to_latex(index=False,
                                              formatters=formatters))


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description=
        """This script generates the results shown in the Supplementary Tables 4.
       The table is generated from the file accuracies_and_times_data_frame.pkl file
       that can be found in the 'idtracker.ai_figures_and_tables_data/SupplTab4'
       folder. This file can be generated from the script 'build_dataframe_SupplTable_4.py'
       that is in the folder 'build_data_frames'. The raw data can be obtained from
       idtracker.ai or from the corresponding author of the paper upon request.
       The output latex table is saved in the folder 'idtracker.ai_figures_and_tables_data'
       with the names SupplTab4.tex. SupplTab6.tex and SupplTab7.tex.""")
    parser.add_argument("-atdf", "--accuracies_and_times_data_frame_path", type=str,
                        help="path to the accuracies_and_times_data_frame")
    args = parser.parse_args()
    accuracies_and_times_data_frame = \
        pd.read_pickle(args.accuracies_and_times_data_frame_path)

    print("Accuracies and times table")
    columns_to_include = ['number_of_animals',
                          'image_quality_condition',
                          'protocol',
                          'preprocessing_time',
                          'protocol1_time',
                          'accuracy_protocol1',
                          'protocol2_time',
                          'accuracy_protocol2',
                          'protocol3_time',
                          'accuracy_protocol3',
                          'postprocessing_time',
                          'accuracy',
                          'tracking_time']
    new_columns_names = ['Num of anim.',
                         'condition',
                         'Protocol',
                         'Prep. time',
                         'P1 time',
                         'P1 acc.',
                         'P2 time',
                         'P2 acc.',
                         'P3 time',
                         'P3 acc.',
                         'Post. time',
                         'Accuracy',
                         'Total time']
    formatters = [f_integer,
                  f_string,
                  f_integer,
                  f_string,
                  f_string,
                  f_accuracy,
                  f_string,
                  f_accuracy,
                  f_string,
                  f_accuracy,
                  f_string,
                  f_accuracy,
                  f_string]
    condition = [(m and x and w) or (m and y and v) or z or (u and x and l) or (u and y and m) for (x,y,z,w,v,u,l,m) in zip(
                                                list(accuracies_and_times_data_frame.image_quality_condition == 'resolution_reduction'),
                                                list(accuracies_and_times_data_frame.image_quality_condition == 'gaussian_blurring'),
                                                list(accuracies_and_times_data_frame.image_quality_condition == 'raw'),
                                                list((accuracies_and_times_data_frame.resolution_reduction == 0.5) | (accuracies_and_times_data_frame.resolution_reduction == 0.25)),
                                                list((accuracies_and_times_data_frame.sigma_gaussian_blurring == 1.) | (accuracies_and_times_data_frame.sigma_gaussian_blurring == 4.)),
                                                list((accuracies_and_times_data_frame.number_of_animals == 8) | (accuracies_and_times_data_frame.number_of_animals == 60)),
                                                list((accuracies_and_times_data_frame.resolution_reduction == .75) | (accuracies_and_times_data_frame.resolution_reduction == 0.35)),
                                                list((accuracies_and_times_data_frame.sigma_gaussian_blurring == 2.) | (accuracies_and_times_data_frame.sigma_gaussian_blurring == 4.)))]
    save_folder = os.path.dirname(os.path.dirname(args.accuracies_and_times_data_frame_path))
    write_latex_table_for_subset_dataframe(save_folder,
                                           accuracies_and_times_data_frame,
                                           columns_to_include,
                                           new_columns_names,
                                           formatters, condition,
                                           'SupplTab4')
