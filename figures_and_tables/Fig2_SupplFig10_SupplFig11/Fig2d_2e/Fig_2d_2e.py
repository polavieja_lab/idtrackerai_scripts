from __future__ import absolute_import, print_function, division
import os
import numpy as np
from sys import platform
import matplotlib.pyplot as plt
import seaborn as sns
# sns.set()
from idtrackerai.utils.py_utils import get_spaced_colors_util
from mpl_toolkits.mplot3d import Axes3D
from scipy.signal import hilbert
import seaborn as sns
from peakdetect import peakdetect
import networkx as nx
import pandas as pd
import SimpleHTTPServer
import SocketServer
import webbrowser
import scipy
sns.set_style('ticks')
plt.ion()

if platform == "darwin":
    import matplotlib
    matplotlib.use("TkAgg")


def as_si(x, ndp):
        s = '{x:0.{ndp:d}e}'.format(x=x, ndp=ndp)
        m, e = s.split('e')
        return r'{m:s}\times 10^{{{e:d}}}'.format(m=m, e=int(e))


def init(trajectories_dict_path):
    trajectories = np.load(trajectories_dict_path).item()['trajectories']
    number_of_animals = trajectories.shape[1]
    colors = get_spaced_colors_util(number_of_animals, black=False, norm=True)
    return trajectories, number_of_animals, colors


def get_individual_speed(trajectories):
    """Gets speed of ech individual from the trajectory dictionary outputted by
    idtracker.ai
    """
    individual_velocities = np.diff(trajectories, axis=0)
    return np.linalg.norm(individual_velocities, axis=2)


def moving_average(a, n=5):
    """Performs a moving average on the array a with a window n. Eventual nan
    are masked, then filled with zeros and finally returned with nans in the
    original positions.
    """
    a = np.ma.masked_array(a, np.isnan(a))
    ret = np.cumsum(a.filled(0))
    ret[n:] = ret[n:] - ret[:-n]
    counts = np.cumsum(~a.mask)
    counts[n:] = counts[n:] - counts[:-n]
    ret[~a.mask] /= counts[~a.mask]
    ret[a.mask] = np.nan
    return ret


def hilbert_envelope(a):
    """Computes the magnitude of the Hilbert envelope of the signal a, masks
    eventual nans
    """
    a = np.ma.masked_array(a, np.isnan(a))
    env = np.abs(hilbert(a.filled(0)))
    env[a.mask] = np.nan
    return env


def smoother(speed, win_length=3):
    """Returns a smoothed signal per individual by using moving_average
    """
    return np.swapaxes(np.asarray([moving_average(speed[:, i], win_length)
                                   for i in range(speed.shape[1])]), 0, 1)


def enveloper(speed):
    """Computes envelope per individual by using hilbert_envelope
    """
    return np.swapaxes(np.asarray([hilbert_envelope(speed[:, i])
                                   for i in range(speed.shape[1])]), 0, 1)


def softmax(x):
    """Compute softmax
    """
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum(axis=0)


def softmaxer(speed):
    """Computes softmax per frame substituting eventual nans with zeros.
    The idea is that if all or a subset of the individuals have the same level
    of activity they will share the same softmaxed values. Individuals whose
    activity cannot be computed will have softmaxed activity equal to zero.
    """
    ret = []

    for frame_speed in speed:
        frame_speed = [s if not np.isnan(s) else 0 for s in frame_speed]
        ret.append(softmax(frame_speed))

    return np.asarray(ret)


def plot_activity_in_time(trajectories, individual_activity, number_of_animals,
                          colors, smooth_window=None, envelope=True,
                          softmaxed=True, interaction_radius=None,
                          save_folder='./'):
    """Main routine
    """
    raw_activity = np.copy(individual_activity)
    # fig = plt.figure()
    # ax = fig.add_subplot(111, projection='3d')
    if smooth_window is not None:
        individual_activity = smoother(individual_activity,
                                       win_length=smooth_window)
    if envelope:
        individual_activity = enveloper(individual_activity)
    if softmaxed:
        individual_activity_s = softmaxer(individual_activity)

    if softmaxed:
        activity_peaks = find_activity_peaks(individual_activity_s,
                                             20,
                                             1/number_of_animals)
        print("act ", activity_peaks.shape)
        interaction_dictionaries = \
            count_interactions_per_individual(
                activity_peaks, trajectories,
                right_neighbour_tolerance=smooth_window,
                interaction_radius=interaction_radius)
        G = build_social_network(interaction_dictionaries, colors, save_folder)
        plot_correlation_activity(interaction_dictionaries,
                                  raw_activity, colors, save_folder)

    plt.show()
    return G


def find_activity_peaks(individual_activity, lookahead, delta, plot_ax=None):
    """Finds extrema per individual from the collection of individual_activity
    that has shape [number_of_frames, number_of_individuals]. Two consecutive
    peaks must be at least at distance lookahead and at least bigger than delta
    """
    maxima_per_individual = []
    minima_per_individual = []
    number_of_animals = individual_activity.shape[1]

    for i in range(number_of_animals):
        maxima, minima = peakdetect(individual_activity[:, i],
                                    lookahead=lookahead,
                                    delta=delta)
        maxima_per_individual.append(maxima)
        minima_per_individual.append(minima)

    activity_peaks = np.zeros_like(individual_activity)

    for i, max_list in enumerate(maxima_per_individual):
        for argmax, maximum in max_list:
            activity_peaks[argmax, i] = 1

    for i, min_list in enumerate(minima_per_individual):
        for argmin, minimum in min_list:
            activity_peaks[argmin, i] = -1

    return activity_peaks


def count_interactions_per_individual(activity_peaks, trajectories,
                                      right_neighbour_tolerance=59,
                                      interaction_radius=200):
    """Counts the interactions between individuals by considering maxima in
    their softmaxed activity. The frame-window at which an interaction is
    considered a response is right_neighbour_tolerance. Furthermore, a
    candidate_answer is a true answer if and only if the triggerer and
    answering animals are at most interaction_radius distance.
    """
    number_of_animals = activity_peaks.shape[1]
    interaction_dictionaries = []

    for i in range(number_of_animals):
        interaction_dictionary = {k + 1: 0 for k in range(number_of_animals)}
        max_act_i = np.where(activity_peaks[:, i] == 1)[0]
        activity_peaks_minus_i = np.copy(activity_peaks)  # np.delete(activity_peaks, i, axis = 1)
        activity_peaks_minus_i[:, i] = 0

        for frame in max_act_i:
            interval = [frame, frame + right_neighbour_tolerance]\
                if frame + right_neighbour_tolerance < activity_peaks.shape[0]\
                else [frame, activity_peaks.shape[0]]
            activity_peaks_slice = \
                activity_peaks_minus_i[interval[0]:interval[1], :]
            interactions = np.where(activity_peaks_slice == 1)
            interacting_frames = interactions[0] + interval[0]
            interacting_individuals = interactions[1]

            for interacting_ind, interaction_frame in zip(interacting_individuals, interacting_frames):
                if np.linalg.norm(trajectories[frame, i] - trajectories[interaction_frame, interacting_ind]) < interaction_radius:
                    interaction_dictionary[interacting_ind + 1] += 1

        interaction_dictionaries.append(interaction_dictionary)

    return interaction_dictionaries


def build_social_network(interaction_dictionaries, colors, save_folder):
    """Builds social network by considering the interaction counts stored in
    interaction_dictionaries.
    """
    G = nx.DiGraph()

    for i, d in enumerate(interaction_dictionaries):
        [G.add_edge(str(i + 1), str(k), weight=d[k], color=colors[i])
         for k in d if d[k] > 1]

    fig, ax = plt.subplots(1, figsize=(5, 5))
    pos = nx.circular_layout(G)
    colors_ = [colors[int(i) - 1] for i in G.nodes]
    colors__ = [G[u][v]['color'] for u, v in G.edges()]
    weights = [G[u][v]['weight'] for u, v in G.edges()]
    nx.draw(G, pos=pos, ax=ax, with_labels=True, node_color=colors_,
            node_size=600,
            width=weights, edge_color=colors__)
    fig.savefig(os.path.join(save_folder, 'Fig2d.pdf'))
    return G


def plot_correlation_activity(interaction_dictionaries, activity, colors, save_folder):
    fig, ax = plt.subplots(1, figsize=(6,3))
    plt.subplots_adjust(left=.15, bottom=.2, right=.85, top=None,
                        wspace=.5, hspace=.3)
    number_of_animals = len(interaction_dictionaries)
    x_reg = []
    y_reg = []

    for i in range(number_of_animals):
        c = np.asarray(colors[i])
        mean_ind_activity = np.nanmedian(activity[:, i])
        x_reg.append(mean_ind_activity)
        interaction_score = sum(interaction_dictionaries[i].values())
        y_reg.append(interaction_score)
        ax.plot(mean_ind_activity, interaction_score, marker='o', c=c)
        plt.text(mean_ind_activity, interaction_score, str(i + 1),
                 color="black", fontsize=14)
        plt.margins(0.1)
        plt.xlabel('mean speed', fontsize=16)
        plt.ylabel('number of \ninititated actions', fontsize=16)
        sns.despine(ax=ax, top=True, right=True)

    slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x_reg,
                                                                         y_reg)
    x = np.linspace(min(min(x_reg), min(x_reg)), max(max(x_reg), max(x_reg)))
    ax.plot(x, slope * x + intercept, color='.5', linestyle='--')
    print("r_value ", r_value**2)
    print("p_value ", p_value)
    min_x = ax.get_xlim()[0]
    max_y = ax.get_ylim()[1]
    min_y = ax.get_ylim()[0]
    ax.text(min_x, max_y - (max_y - min_y)*.1,
            r'$R^2$ = %.4f' % r_value**2, fontsize=10)
    ax.text(min_x, max_y - (max_y - min_y)*.2,
            r"$p = {0:s}$".format(as_si(p_value, 0)), fontsize=10)
    fig.savefig(os.path.join(save_folder, 'Fig2e.pdf'))


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description=
        """This script generates the Figures 2d and 2e from the trajectories_wo_gaps.npy
        files that can be found in the folder 'tracked_videos/ants_andrew_1/session_20180206/trajectories_wo_gaps'.""")
    parser.add_argument("-tvfp", "--tracked_videos_folder_path", type=str, default=None,
                        help="Path to the tracked_videos folder")
    args = parser.parse_args()

    tracked_videos_folder_path = args.tracked_videos_folder_path
    save_folder = os.path.dirname(tracked_videos_folder_path)
    trajectories_dict_path = os.path.join(tracked_videos_folder_path,
        'ants_andrew_1/session_20180206/trajectories_wo_gaps/trajectories_wo_gaps.npy')
    video_object_path = os.path.join(tracked_videos_folder_path,
        'ants_andrew_1/session_20180206/video_object.npy')
    video_object = np.load(video_object_path).item()
    interaction_radius = 3 * video_object.median_body_length
    one_second_smoothing = video_object.frames_per_second
    trajectories, number_of_animals, colors = init(trajectories_dict_path)
    speed = get_individual_speed(trajectories)
    speed = np.vstack((np.zeros((1, number_of_animals)), speed))
    G = plot_activity_in_time(trajectories, speed,
                              number_of_animals, colors,
                              smooth_window=one_second_smoothing,
                              envelope=True,
                              softmaxed=True,
                              interaction_radius=interaction_radius,
                              save_folder=save_folder)
