from argparse import Namespace
import numpy as np
from scipy.ndimage.filters import gaussian_filter1d


def calculate_center_of_mass(trajectories):
    center_of_mass = Namespace()
    center_of_mass_dict = vars(center_of_mass)
    trajectories_dict = vars(trajectories)
    center_of_mass_dict.update(
        {k: np.nanmean(v, axis=1) for k, v in trajectories_dict.items()})
    return center_of_mass


def norm(a, keepdims=False):
    return np.linalg.norm(a, axis=-1, keepdims=keepdims)


def normalise(a):
    return a / norm(a)[..., np.newaxis]


def curvature(v, a):
    assert(v.shape[-1]) == 2
    assert(np.all(v.shape == a.shape))
    return (v[..., 0]*a[..., 1]-v[..., 1]*a[..., 0])/np.power(norm(v), 3)


def dot(x, y, keepdims=False):
    result = np.einsum('...i,...i->...', x, y)
    if keepdims:
        return np.expand_dims(result, -1)
    else:
        return result


def _interpolate_nans(t):
    """Interpolates nans linearly in a trajectory

    :param t: trajectory
    :returns: interpolated trajectory
    """
    shape_t = t.shape
    reshaped_t = t.reshape((shape_t[0], -1))
    for timeseries in range(reshaped_t.shape[-1]):
        y = reshaped_t[:, timeseries]
        nans, x = _nan_helper(y)
        y[nans] = np.interp(x(nans), x(~nans), y[~nans])

    # Ugly slow hack, as reshape seems not to return a view always
    back_t = reshaped_t.reshape(shape_t)
    t[...] = back_t


def _nan_helper(y):
    """Helper to handle indices and logical indices of NaNs.
    https://stackoverflow.com/questions/6518811/interpolate-nan-values-in-a-numpy-array
    Input:
        - y, 1d numpy array with possible NaNs
    Output:
        - nans, logical indices of NaNs
        - index, a function, with signature indices= index(logical_indices),
          to convert logical indices of NaNs to 'equivalent' indices
    Example:
        >>> # linear interpolation of NaNs
        >>> nans, x= nan_helper(y)
        >>> y[nans]= np.interp(x(nans), x(~nans), y[~nans])
    """

    return np.isnan(y), lambda z: z.nonzero()[0]


def find_center(t):
    """Find center of trajectories

    TODO: Change this to something more sophisticated

    :param t: trajectory
    """
    center_x = (np.nanmax(t[..., 0]) + np.nanmin(t[..., 0]))/2
    center_y = (np.nanmax(t[..., 1]) + np.nanmin(t[..., 1]))/2
    return center_x, center_y


def normalise_trajectories(t):
    """Normalise trajectories in place so their values are between -1 and 1

    :param t: trajectory, to be modified in place. Last dimension is (x,y)
    :returns: scale and shift to recover unnormalised trajectory
    """
    norm_const_x = (np.nanmax(t[..., 0]) - np.nanmin(t[..., 0]))/2
    norm_const_y = (np.nanmax(t[..., 1]) - np.nanmin(t[..., 1]))/2
    norm_const = max(norm_const_x, norm_const_y)

    center_x, center_y = find_center(t)

    t[..., 0] -= center_x
    t[..., 1] -= center_y
    np.divide(t, norm_const, t)
    return norm_const, center_x, center_y


def smooth_several(t, sigma=2, truncate=5, derivatives=[0]):
    return [smooth(t, sigma=sigma, truncate=truncate,
                   derivative=derivative) for derivative in derivatives]


def smooth(t, sigma=2, truncate=5, derivative=0):
    smoothed = gaussian_filter1d(t, sigma=sigma, axis=0,
                                 truncate=truncate, order=derivative)
    return smoothed


def smooth_velocity(t, **kwargs):
    kwargs['derivative'] = 1
    return smooth(t, **kwargs)


def smooth_acceleration(t, **kwargs):
    kwargs['derivative'] = 2
    return smooth(t, **kwargs)


def velocity_acceleration_backwards(t, k_v_history=0.01):
    v = (1-k_v_history)*(t[2:] - t[1:-1]) + k_v_history*(t[1:-1] - t[:-2])
    a = t[2:] - 2*t[1:-1] + t[:-2]
    return t[2:], v, a


def velocity_acceleration(t):
    v = (t[2:] - t[:-2])/2
    a = t[2:] - 2*t[1:-1] + t[:-2]
    return t[1:-1], v, a


class Trajectories():
    def __init__(self, trajectories):
        self.trajectories = trajectories
        self.center_of_mass = calculate_center_of_mass(trajectories)
        self.__dict__.update(vars(self.trajectories))

    def __getitem__(self, val):
        view_trajectories = Namespace()
        vars(view_trajectories).update(
            {k: v[val] for k, v in vars(self.trajectories).items()})
        return Trajectories(view_trajectories)

    def view(self, start=None, end=None):
        return self[slice(start, end)]

    @classmethod
    def from_idtracker(cls, trajectories_path,
                       interpolate_nans=True, normalise=True,
                       smooth_sigma=0, diff_backwards=True, dtype=np.float64):
        traj_dict = np.load(trajectories_path, encoding='latin1').item()
        # Bring here the properties that we need from the dictionary
        t = traj_dict['trajectories'].astype(dtype)
        return cls.from_positions(t, interpolate_nans=interpolate_nans,
                                  smooth_sigma=smooth_sigma,
                                  diff_backwards=diff_backwards)

    @classmethod
    def from_positions(cls, t, interpolate_nans=True, smooth_sigma=0,
                       diff_backwards=True):
        trajectories = Namespace()
        trajectories.raw = t.copy()
        normalise_trajectories(t)
        if interpolate_nans:
            _interpolate_nans(t)
        if smooth_sigma > 0:
            t_smooth = smooth(t, sigma=smooth_sigma)
        else:
            t_smooth = t
        # diff_backwards = False not implemented
        if diff_backwards:
            [trajectories.s, trajectories.v, trajectories.a] = \
                velocity_acceleration_backwards(t_smooth)
        else:
            [trajectories.s, trajectories.v, trajectories.a] = \
                velocity_acceleration(t_smooth)

        trajectories.speed = norm(trajectories.v)
        trajectories.acceleration = norm(trajectories.a)
        trajectories.distance_to_center = norm(trajectories.s)
        trajectories.e = normalise(trajectories.v)
        trajectories.tg_acceleration = dot(trajectories.a, trajectories.e)
        trajectories.curvature = curvature(trajectories.v, trajectories.a)
        trajectories.normal_acceleration = \
            np.square(trajectories.speed)*trajectories.curvature
        return cls(trajectories)

    @property
    def number_of_frames(self):
        return self.s.shape[0]

    @property
    def number_of_individuals(self):
        return self.s.shape[1]

    @property
    def identity_labels(self):
        # Placeholder, in case in the future labels are explicitly given
        return np.arange(self.number_of_individuals)

    @property
    def identities_array(self):
        ones = np.ones(self.raw.shape[:-1], dtype=np.int)
        return np.einsum('ij,j->ij', ones, self.identity_labels)
