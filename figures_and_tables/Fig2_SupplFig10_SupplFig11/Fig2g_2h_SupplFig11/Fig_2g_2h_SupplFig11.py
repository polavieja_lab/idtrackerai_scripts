import argparse
import os
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import seaborn as sns
import scipy
from trajectorytools import Trajectories

RADIUS = 33  # cm
FRAME_RATE = 32  # fps
VIDEO_INTERVAL = [0, 19000]  # MAX IS 19000
STRIDE = FRAME_RATE
NUMBER_OF_NEIGHBOURS = 99
FRAMES_TO_COMPUTE_TAU = FRAME_RATE * 10
TAU_RANGE = FRAME_RATE * 2  # number of frames used for the delay
CORRELATION_THRESHOLD = 0.8


def get_spaced_colors(n, cmap='jet'):
    RGB_tuples = matplotlib.cm.get_cmap(cmap)
    return [RGB_tuples(i / n) for i in range(n)]


def plot_individual_distribution_of_vector(ax_arr, vector,
                                           identities,
                                           nbins=25, ticks=False,
                                           V_max=None):
    if np.isnan(vector).any():
        print('Removing NaNs from data')
    v_max = 0
    min_x, max_x = -RADIUS, RADIUS
    min_y, max_y = -RADIUS, RADIUS
    X, Y = np.mgrid[min_x:max_x:50j, min_y:max_y:50j]
    positions = np.vstack([X.ravel(), Y.ravel()])
    G_kernels = []
    for i, identity in enumerate(identities):
        print("computing G kernel for identity ", identity)
        values = vector[:, identity-1, :].T
        kernel = scipy.stats.gaussian_kde(values)
        Z = np.reshape(kernel(positions).T, X.shape)
        G_kernels.append(Z)
        v_max = max(v_max, G_kernels[i].max())
    v_max = v_max if V_max is None else V_max
    print(v_max)
    for i, identity in enumerate(identities):
        ax_arr[i].imshow(np.rot90(G_kernels[i]), cmap='Greys',
                         extent=[min_x, max_x, min_y, max_y],
                         vmin=0, vmax=v_max)
        ax_arr[i].set_title('individual %i' % identity, fontsize=14)
        ax_arr[i].set_xlabel('X position (cm)')
        ax_arr[i].set_ylabel('Y position (cm)')

        ax_arr[i].invert_yaxis()


def plot_correlation_two_variables(ax, x, y, identities, x_label, y_label):
    def as_si(x, ndp):
        s = '{x:0.{ndp:d}e}'.format(x=x, ndp=ndp)
        m, e = s.split('e')
        return r'{m:s}\times 10^{{{e:d}}}'.format(m=m, e=int(e))

    number_of_individuals = x.shape[0]
    for i in range(number_of_individuals):
        if i+1 not in identities:
            ax.plot(x[i], y[i], 'k.')
    markers = ["v", "o", "^"]
    for i, identity in enumerate(identities):
        ax.plot(x[identity-1], y[identity-1], color='.5', marker=markers[i])

    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x, y)
    x = np.linspace(min(x.min(), x.min()), max(x.max(), x.max()))
    ax.plot(x, slope * x + intercept, color='b', linestyle='--')
    print("slope ", slope)
    print("intercept ", intercept)
    print("r_value", r_value**2)
    print("p_value", p_value)
    min_x = ax.get_xlim()[0]
    max_y = ax.get_ylim()[1]
    min_y = ax.get_ylim()[0]
    print(min_y, max_y)
    ax.text(min_x, max_y - (max_y - min_y)*.1,
            r'$R^2$ = %.4f' % r_value**2,
            fontsize=10)
    ax.text(min_x, max_y - (max_y - min_y)*.2,
            r"$p = {0:s}$".format(as_si(p_value, 0)),
            fontsize=10)


def speed_vs_distance_to_center_no_identity(trajectories_path, results_folder):
    import pandas as pd
    window = 500
    t = Trajectories.from_idtracker(trajectories_path)
    colors = np.asarray(get_spaced_colors(t.number_of_individuals))
    plt.ion()
    fig, ax = plt.subplots(1, 1)
    for i in range(t.s.shape[1]):
        ax.plot(pd.rolling_mean(t.distance_to_center[:, i], window),
                pd.rolling_mean(t.speed[:, i], window),
                '.k', alpha=0.006, markersize=1)
    distance_to_center = np.mean(t.distance_to_center, axis=0)
    av_speed = np.mean(t.speed, axis=0)
    print(np.max(av_speed), np.min(av_speed))
    for i in range(t.number_of_individuals):
        ax.plot(distance_to_center[i], av_speed[i],
                'k+', markeredgewidth=1, color=colors[i])
    ax.set_xlabel('Mean distance to center')
    ax.set_ylabel('Mean speed')
    plt.show()


def main_figure(files_dict, group_size, group, save_folder):
    # Load trajectories
    trajectories_path = os.path.expanduser(files_dict[group_size][group])
    t = Trajectories.from_idtracker(trajectories_path)
    # Average distance to the center of the arena
    average_distance_to_center = np.nanmean(t.distance_to_center, axis=0)
    # Average speed
    average_speed = np.nanmean(t.speed, axis=0)

    # Plotting setup
    sns.set_style('ticks')
    plt.ion()
    fig, ax_arr = plt.subplots(1, 4, figsize=(13, 4))
    plt.subplots_adjust(left=.15, bottom=.3, right=.85, top=.7,
                        wspace=.3, hspace=.5)

    # Position distributions plots
    indices = np.argsort(average_distance_to_center)
    identities = [indices[0] + 1, indices[len(indices)//2] + 1,
                  indices[-1] + 1]
    plot_individual_distribution_of_vector(ax_arr[:3], t.s * RADIUS,
                                           identities, V_max=0.00075)

    # Correlation plot
    plot_correlation_two_variables(ax_arr[3],
                                   average_distance_to_center * RADIUS,
                                   average_speed * RADIUS * FRAME_RATE,
                                   identities,
                                   'Mean distance to center (cm)',
                                   'Mean speed (cm/s)')
    # Axes settings
    ax_arr[1].set_yticklabels([])
    ax_arr[1].set_ylabel('')
    ax_arr[2].set_yticklabels([])
    ax_arr[2].set_ylabel('')
    sns.despine(ax=ax_arr[3], top=True, right=True)
    fig.savefig(os.path.join(save_folder, 'Fig2g_2h.pdf'))


def supplementary_figure(files_dict, group_sizes, groups,save_folder):
    # Plotting setup
    sns.set_style('ticks')
    plt.ion()
    fig, ax_arr = plt.subplots(len(group_sizes), 4, figsize=(10, 4.5))
    plt.subplots_adjust(left=.1, bottom=None, right=.9, top=None,
                        wspace=.5, hspace=.3)

    max_av_distance_to_center = 0
    min_av_distance_to_center = RADIUS
    max_av_speed = 0
    min_av_speed = 100
    average_distances_to_center = []
    average_speeds = []
    selected_identities = []
    for i, (group_size, group) in enumerate(zip(group_sizes, groups)):
        # Load trajectories
        trajectories_path = os.path.expanduser(files_dict[group_size][group])
        t = Trajectories.from_idtracker(trajectories_path)
        # Average distance to the center of the arena
        average_distance_to_center = np.nanmean(t.distance_to_center, axis=0)
        average_distances_to_center.append(average_distance_to_center)
        max_av_distance_to_center = max(max_av_distance_to_center,
                                        max(average_distance_to_center))
        min_av_distance_to_center = min(min_av_distance_to_center,
                                        min(average_distance_to_center))
        # Average speed
        average_speed = np.nanmean(t.speed, axis=0)
        average_speeds.append(average_speed)
        max_av_speed = max(max_av_speed, max(average_speed))
        min_av_speed = min(min_av_speed, min(average_speed))
        # Position distributions plots
        indices = np.argsort(average_distance_to_center)
        identities = [indices[0] + 1, indices[len(indices)//2] + 1,
                      indices[-1] + 1]
        selected_identities.append(identities)
        plot_individual_distribution_of_vector(ax_arr[i, :3], t.s*RADIUS,
                                               identities,
                                               V_max=0.00075)

    for i in range(len(group_sizes)):
        # Correlation plot
        ax_arr[i, 3].set_xlim((min_av_distance_to_center*.8*RADIUS,
                               max_av_distance_to_center*1.2*RADIUS))
        ax_arr[i, 3].set_ylim((min_av_speed*.8 * RADIUS * FRAME_RATE,
                               max_av_speed*1.2 * RADIUS * FRAME_RATE))

        plot_correlation_two_variables(ax_arr[i, 3],
                                       average_distances_to_center[i]*RADIUS,
                                       average_speeds[i]*RADIUS*FRAME_RATE,
                                       selected_identities[i],
                                       'Mean distance to center (cm)',
                                       'Mean speed (cm/s)')
        # Axes settings
        ax_arr[i, 1].set_yticklabels([])
        ax_arr[i, 1].set_ylabel('')
        ax_arr[i, 2].set_yticklabels([])
        ax_arr[i, 2].set_ylabel('')
        sns.despine(ax=ax_arr[i, 3], top=True, right=True)
        if i != len(group_sizes)-1:
            for ax in ax_arr[i, :]:
                ax.set_xlabel('')
                ax.set_xticklabels([])
    fig.savefig(os.path.join(save_folder, 'SuppleFig11.pdf'))


def main():
    parser = argparse.ArgumentParser(description=
         """This script generates the panels Fig2g and 2h of the paper and the
         Supplementary Figure 11. The plots are generated from the trajectories.npy
         files for the three videos of 100 fish. Those files can be found in
         'tracked_videos' folder""")
    parser.add_argument('-tvfp', '--tracked_videos_folder_path', type=str, default=None,
                        help='path to tracked_videos folder')
    args = parser.parse_args()
    tracked_videos_folder_path = args.tracked_videos_folder_path
    files_dict = {100: [
        os.path.join(tracked_videos_folder_path, 'idTrackerDeep_LargeGroups_1/100/',
                     'First/session_20180102/trajectories/trajectories.npy'),
        os.path.join(tracked_videos_folder_path,
                     'idTrackerDeep_LargeGroups_2/TU20170307/',
                     'numberIndivs_100/First/session_20180104/',
                     'trajectories/trajectories.npy'),
        os.path.join(tracked_videos_folder_path,
                     'idTrackerDeep_LargeGroups_3/100fish/',
                     'First/session_02122017/trajectories/trajectories.npy')]}
    save_folder = os.path.dirname(tracked_videos_folder_path)
    main_figure(files_dict, 100, 2, save_folder)
    supplementary_figure(files_dict, [100, 100], [0, 1], save_folder)


if __name__ == '__main__':
    main()
