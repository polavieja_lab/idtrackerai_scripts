% makeAggressionScore: Calculate attack scores for fighting zebrafish pairs

% figdirectory =  'F:\agression_zebrafish\MATLAB\Figures\'; % Where should figures be saved?
% trroot =  'F:\agression_zebrafish\MATLAB\'; % Where are the trajecory files stored? in the hard drive idTracker_SmallGroups_1

trroot = input('path to the agression_data folder in the SupplFig_10_data folder: ');

% Video parameters
bodylength = 90; % Approx. length of animals in pixels
framerate = 30; % Frames per second

% List of trajectory paths. The idtracker.ai output files (.npy) are converted to Matlab .mat files.
trlocList = {[trroot '\Friday070417\idtrackerAI\row1col1\trajectories_wo_gaps.mat']; ...
    [trroot '\Friday070417\idtrackerAI\row1col2\trajectories_wo_gaps.mat']; ...
    [trroot '\Friday070417\idtrackerAI\row2col1\trajectories_wo_gaps.mat']; ...
    [trroot '\Friday070417\idtrackerAI\row2col2\trajectories_wo_gaps.mat']; ...
    [trroot '\Tuesday280317\idtrackerAI\row1col1\trajectories_wo_gaps.mat']; ...
    [trroot '\Tuesday280317\idtrackerAI\row1col2\trajectories_wo_gaps.mat']; ...
    [trroot '\Tuesday280317\idtrackerAI\row2col1\trajectories_wo_gaps.mat']; ...
    [trroot '\Tuesday280317\idtrackerAI\row2col2\trajectories_wo_gaps.mat']; ...
    [trroot '\Tuesday170117\idtrackerAI\row1col1\trajectories_wo_gaps.mat']; ...
    [trroot '\Tuesday170117\idtrackerAI\row1col2\trajectories_wo_gaps.mat']; ...
    [trroot '\Tuesday170117\idtrackerAI\row2col1\trajectories_wo_gaps.mat']; ...
    };

% The indeces of winners of each pair, chosen as the animal which has the
% higher attack score at the end of the video
winner_list_aiID = [2 1 1 1 2 2 1 2 2 2 2];


% Select trials for main text and supplementary figure
main_idx = [2 7 9];
suppl_idx = [6 5 10 4 3 1 8 11];


% Some fixed parameters
no_fish = 2;
no_trials = size(trlocList,1);
no_trials_main = numel(main_idx);
no_trials_suppl = numel(suppl_idx);
no_frames_array = NaN(no_trials,1);


aggressionScoreCell = cell(no_trials,1); % Will contain the #frames x #fish x #fish array of attack scores for each trial
for trial = 1:no_trials
    trdirectory = trlocList{trial};
    trajectoriesAll = load(trdirectory);
    trajectories = trajectoriesAll.trajectories;
    no_frames = size(trajectories,1);
    no_frames_array(trial) = no_frames; % Store #frames for each trial. Can vary, not all videos have the same length
    no_dim = size(trajectories,3);


    %% Calculate velocity and acceleration

    % Attack - Definiton. An attack takes place when:
    filter_neighborfrontangle = pi/4; % > Angle between focal movement direction and neighbor position
    filter_focalspeed_bl_s = 1.5; % > the speed of the focal
    filter_neighbordistance_bl = 2; % > distance between focal and neighbor

    % Calculate the aggression frames:
    aggressionScoreTemp = trajectories2attacks(trajectories,bodylength,framerate, ...
        filter_neighborfrontangle, filter_focalspeed_bl_s, filter_neighbordistance_bl);

    % Take care of "NaNs": They should neither count as an attack nor as
    % not-an-attack. Make sure NaN-frames in trajectories are also
    % NaN-frames in the aggression score
    for ff = 1:no_fish
        for nf = 1:no_fish
            nanidx = squeeze(any(isnan(trajectories(:,ff,:)),3));
            aggressionScoreTemp(nanidx,ff,nf) = NaN;
        end
    end

    aggressionScoreCell{trial} = squeeze(aggressionScoreTemp);

end

max_no_frames = cellfun(@(x) size(x,1), aggressionScoreCell);
max_no_frames = max(max_no_frames(:));

aggressionScore = NaN(no_trials,max_no_frames,no_fish,no_fish);
for trial = 1:no_trials

    act_no_frame = size(aggressionScoreCell{trial},1);
    aggressionScore(trial,1:act_no_frame,:,:) = aggressionScoreCell{trial};

end
%% Write csv files with the results for the main figure
count = 1;
numlist = 'a':'z';
time_window_frames = 3600;
for trial = main_idx
    focal = winner_list_aiID(trial);
    neighbor = setxor(1:2,winner_list_aiID(trial));

    tt = squeeze(aggressionScore(trial,1:no_frames_array(trial),focal,neighbor));
    nanidx = isnan(tt);
    tt(~nanidx) = smooth(tt(~nanidx),time_window_frames,'moving');
    tt = tt(time_window_frames+1:end-time_window_frames);

    tt2 = squeeze(aggressionScore(trial,1:no_frames_array(trial),neighbor,focal));
    nanidx = isnan(tt2);
    tt2(~nanidx) = smooth(tt2(~nanidx),time_window_frames,'moving');
    tt2 = tt2(time_window_frames+1:end-time_window_frames);

    csvwrite([trroot 'attackScore_' numlist(count) '.csv'],[tt' tt2'])
    count = count + 1;
end

%% Supplementary figure
time_window_frames = 3600; % The attack score is calculated as the mean number of frames in which an attack is present in a time window of "time_window_frames"

% Some design parameters:
linewidth_plot = 1.5;
linewidth_axes = 1.5;
fontsize_axeslabel = 12;
fontsize_enum = 14;
xlim = [-.5 50.5];
ylim = [-0.01 .7];

right_margin = 3;
left_margin = 2;
bottom_margin = 2;
top_margin = 0.5;
horz_margin_between_axes = .5;
vert_margin_between_axes = .5;
no_rows = ceil(no_trials_suppl/2);
no_cols = 2;

figure_size=[210 170]/10;
set(0,'Units','inches')
screensize_cm = get(0,'screensize')*2.54;
set(0,'units','pixels')
screensize_cm = screensize_cm(3:4);
figure_position=[screensize_cm(1)-24 0 figure_size(1) figure_size(2)];
plot_area_width=figure_position(3)-right_margin-left_margin;
fh=figure('Unit','centimeters','Position',figure_position,'Color','w','Renderer','opengl');
axes_width=(plot_area_width-horz_margin_between_axes*(1-1))/2;
row_height=(figure_size(2)-bottom_margin-top_margin-vert_margin_between_axes*(no_rows-1))/(no_rows);

[X,Y] = meshgrid(left_margin+(axes_width+horz_margin_between_axes)*(0:no_cols-1), ...
    figure_size(2) - top_margin - row_height*(1:no_rows) - vert_margin_between_axes*(0:no_rows-1));

% winner_supl = winner_list_aiID(suppl_idx);
count = 1;
supplH = NaN(1,no_trials_suppl);
enum_list = 'a':'z';
for trial = suppl_idx
    col = mod(count-1, no_cols)+1;
    row = ceil(count/no_cols);
    supplH(count) = axes('Unit','centimeters','Position',[X(row, col) Y(row,col) axes_width row_height]);

    for focal = [winner_list_aiID(trial) setxor(1:2,winner_list_aiID(trial))];
        ax = gca;
        hold on
        neighbor = setxor(1:2,focal);
        tt = squeeze(aggressionScore(trial,1:no_frames_array(trial),focal,neighbor));
        nanidx = isnan(tt);
        tt(~nanidx) = smooth(tt(~nanidx),time_window_frames,'moving');
        tt = tt(time_window_frames+1:end-time_window_frames);
        plot(((1:numel(tt))-1)/framerate/60, ...
            tt,'-','LineWidth',linewidth_plot)
        set(gca,'FontSize',12,'YLim',[0 .7])
    end

    set(gca,'XTick',0:10:60,'YTick',0:.2:1,'XTickLabel',0:10:60,'LineWidth',linewidth_axes,'XLim',xlim,'YLim',ylim,'TickDir','out')
    if row ~=no_rows %&& count ~= no_rows*no_cols-(no_cols)
        set(gca,'XTickLabel','')
    elseif row == no_rows || count == no_rows*no_cols-(no_cols)

        xlabel(supplH(count),'Time (min)')

    end
    if col ~= no_cols
        ylabel('Attack score','FontSize',fontsize_axeslabel)
    else
        set(gca,'YTickLabel','')
    end
%     text(1,.65,[enum_list(count) ')'],'FontSize',fontsize_enum)
    count = count + 1;

end

%% Main text figure
% Stage 1
cone_flag = false; % If true, a cone will be drawn on top and in front of the focal animal to indicate the definition of attacks
attackscore_flag = true; % If true, attack scores for 3 pairs will be plotted

time_window_frames = 3600;
linewidth_plot = 1.5;
linewidth_axes = 1.5;
fontsize_axeslabel = 14;
fontsize_axis = 12;
linewidth_arrow = 2;
xlim = [-1 50.5];

ylim = [-0.01 .5];

right_margin = 0;
left_margin = 0;
bottom_margin = 40;
top_margin = 0;
horz_margin_between_axes = 15;
vert_margin_between_axes = 20;
no_rows = 3;
no_cols = 1;

figure_size=[500.47718 185.84344];

set(0,'Units','inches')
screensize_cm = get(0,'screensize')*2.54;
set(0,'units','pixels')
screensize_cm = screensize_cm(3:4);
if attackscore_flag
    figure_position=[10 0 500.47718 185.84344];
else
    figure_position=[10 0 500.47718/3-right_margin-horz_margin_between_axes*2-10 185.84344];
end
plot_area_width=figure_position(3)-right_margin-left_margin;
fh=figure('Unit','pixels','Position',figure_position,'Color','w','Renderer','opengl');
axes_width=(plot_area_width-10-horz_margin_between_axes*(3-1))/3;
row_height=(figure_size(2)-bottom_margin-top_margin-vert_margin_between_axes*(no_rows-1))/(no_rows);

[X,Y] = meshgrid(left_margin+(axes_width+horz_margin_between_axes-7)*(1:no_cols), ...
    figure_size(2) - top_margin - row_height*(1:no_rows) - vert_margin_between_axes*(0:no_rows-1));
X = X + 20;

if attackscore_flag
count = 1;
mainH = NaN(1,no_trials_main+1);
for trial = main_idx
    col = mod(count-1, no_cols)+1;
    row = ceil(count/no_cols);
    mainH(count+1) = axes('Unit','pixels','Position',[X(row, col) Y(row,col) axes_width row_height]);

    for focal = [winner_list_aiID(trial) setxor(1:2,winner_list_aiID(trial))];
        ax = gca;
        hold on
        neighbor = setxor(1:2,focal);
        tt = squeeze(aggressionScore(trial,1:no_frames_array(trial),focal(1),neighbor));
        nanidx = isnan(tt);
        tt(~nanidx) = smooth(tt(~nanidx),time_window_frames,'moving');
        tt = tt(time_window_frames+1:end-time_window_frames);
        plot(((1:numel(tt))-1)/framerate/60, ...
            tt,'-','LineWidth',linewidth_plot)
        set(gca,'FontSize',fontsize_axis,'YLim',[0 .7])
    end

    set(gca,'XTick',0:10:60,'YTick',0:.2:1,'XTickLabel',0:10:60,'LineWidth',linewidth_axes,'XLim',xlim,'YLim',ylim,'TickDir','out')
    set(gca,'XTickLabel','')
    set(gca,'YTickLabel','')

    if row ~= no_rows
    elseif row == no_rows

        xh=xlabel(mainH(count+1),'Time (min)','FontSize',fontsize_axeslabel);
        actPos = get(xh,'Position');
        set(xh,'Position',[actPos(1) actPos(2)-.07])

        xlbs = 0:10:50;
        for k = 1:numel(xlbs)
            text(xlbs(k),-.03,num2str(xlbs(k)),'FontSize',fontsize_axis,'VerticalAlignment','top','HorizontalAlignment','center','FontWeight','normal')
        end

    end
    if col ~= no_cols && row==no_rows
        yh = ylabel('P(Attack)','FontSize',fontsize_axeslabel);
        actPos = get(yh,'Position');
        set(yh,'Position',[actPos(1)-3 actPos(2)+.4])
    end
    set(gca,'YTickLabel','')
    if col == 1
        ylbs = 0:.2:.4;
        for k = 1:numel(ylbs)
            text(-2,ylbs(k),num2str(ylbs(k)),'FontSize',fontsize_axis,'VerticalAlignment','middle','HorizontalAlignment','right','FontWeight','normal')
        end

    end
    count = count + 1;

end
end % if attackscore_flag

% % Load video frame
% load([figdirectory 'exampleframe.mat'])
% vidHeight = v.Height;
% vidWidth = v.Width;
% v.CurrentTime = 57 + 3/30;
% act_frame = 3600 + 1 + v.CurrentTime*framerate;
% frame = readFrame(v);
%
% trdirectory = trlocList{4};
% trajectoriesAll = load(trdirectory);
% trajectories = trajectoriesAll.trajectories;
% ff = 1;
% nf = 2;
%
% vidX = 700;
% vidY = 547;
% smooth_degree = 30;
% smooth_method = 'moving';
% vel=NaN(size(trajectories));
% vel(1:no_frames-1,:,:)=diff(trajectories,1,1)*framerate;
% vel_magn=sqrt(nansum(vel.^2,3));
% vel_norm=bsxfun(@rdivide,vel,vel_magn);
%
% tr_temp = NaN(size(trajectories));
% for foc=1:no_fish
%     for d=1:no_dim
%         tr_temp(:,foc,d)=smooth(trajectories(:,foc,d),smooth_degree,smooth_method);
%     end
% end
%
% bottom_margin2 = 1;
% top_margin2 = 1;
% left_margin2 =0;%-360;
% mainH(1) = axes('Unit','pixels','Position',[left_margin2 bottom_margin2+.5 figure_size(1)-X(1)-left_margin2 figure_size(2)-bottom_margin2-top_margin2]);
%
% imagesc(frame(vidY:840,vidX:1230))
% frame_size = size(frame(vidY:840,vidX:1230));
% imRat = (vidY-840)/(vidX-1230);
% aHeight =  figure_size(2)-bottom_margin2-top_margin2;
% set(mainH(1),'Position',[left_margin2 bottom_margin2+.5 aHeight/imRat aHeight])
% hold on
% % Plot position
% x = trajectories(act_frame,ff,1)-vidX+1;
% y = trajectories(act_frame,ff,2)-vidY+1;
% ffH = plot(x,y,'.','LineWidth',1.5,'MarkerSize',20);
% nfH = plot(trajectories(act_frame,nf,1)-vidX+1,trajectories(act_frame,nf,2)-vidY+1,'.','LineWidth',1.5,'MarkerSize',20);
%
% % Plot a bit of trajectory
% tail = 15;
% xT = trajectories(act_frame-tail:act_frame,ff,1)-vidX+1;
% yT = trajectories(act_frame-tail:act_frame,ff,2)-vidY+1;
% ffH2 = plot(xT,yT,'.','LineWidth',1.5,'MarkerSize',10,'Color',get(ffH,'Color'));
% plot(trajectories(act_frame-tail:act_frame,nf,1)-vidX+1,trajectories(act_frame-tail:act_frame,nf,2)-vidY+1,'.','LineWidth',1.5,'MarkerSize',10,'Color',get(nfH,'Color'))
%
% if cone_flag
%     % Plot focal speed
%     v = squeeze(vel_norm(act_frame,ff,:));
%     quiver(x+v(1)*bodylength/2.5,y+v(2)*bodylength/2.5, ...
%         v(1)*bodylength*.2, ...
%         v(2)*bodylength*.2,'Color',get(ffH,'Color'),'AutoScaleFactor',1,'MaxHeadSize',15,'Linewidth',linewidth_arrow)
%
%
%     % "Front cone"
%     a = filter_neighborfrontangle;
%     M = [cos(a) -sin(a); sin(a) cos(a)];
%     vR1 = v'*M;
%     xCone = x + [0:vR1(1)/40: vR1(1)]*bodylength*filter_neighbordistance_bl;
%     yCone = y + [0:vR1(2)/40:vR1(2)]*bodylength*filter_neighbordistance_bl;
%
%     xCone(xCone<=0) = 3;
%     yCone(yCone<=0) = 3;
%
%     plot(xCone, ...
%         yCone, ...
%         '-','Linewidth',linewidth_arrow,'Color',get(ffH,'Color'))
%
%     a = -filter_neighborfrontangle;
%     M = [cos(a) -sin(a); sin(a) cos(a)];
%     vR2 = v'*M;
%
%     xCone = x + [0:vR2(1)/40:vR2(1)]*bodylength*filter_neighbordistance_bl;
%     yCone = y + [0:vR2(2)/40: vR2(2)]*bodylength*filter_neighbordistance_bl;
%
%     plot(xCone, ...
%         yCone, ...
%         '-','Linewidth',linewidth_arrow,'Color',get(ffH,'Color'))
%
%     % Distance
%     centerAngle = atan2(v(2),v(1));
%     g1 = atan2(vR1(2),vR1(1));
%     g2 = atan2(vR2(2),vR2(1));
%     xCrc = x + filter_neighbordistance_bl*bodylength*cos(g1:.001:g2);
%     yCrc = y + filter_neighbordistance_bl*bodylength*sin(g1:.001:g2);
%
%     xCrc(xCrc<=1) = 3; xCrc(xCrc>=frame_size(2)) = frame_size(2)-2;
%     yCrc(yCrc<=1) = 3; yCrc(yCrc>=frame_size(1)) = frame_size(1)-2;
%
%     plot(xCrc, ...
%         yCrc, ...
%         '-','Linewidth',linewidth_arrow,'Color',get(ffH,'Color'))
%
% end % if cone_flag
%
% colormap gray
