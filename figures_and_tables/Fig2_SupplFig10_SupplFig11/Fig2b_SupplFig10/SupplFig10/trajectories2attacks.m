function aggressionFrames = trajectories2attacks(trajectories,bodylength,framerate, ...
    filter_neighborfrontangle, filter_focalspeed_bl_s, filter_neighbordistance_bl)
% For each frame in <trajectories>, trajectories2attacks
% determines if an individual attacks a neighbor, where an attack is
% defined as: The focal has a speed >  filter_focalspeed_bl_s bodylength/second,
% the absolute angle between the focal's movement direction and neighbor position <
% filter_neighborfrontangle rad, and the distance between focal and neighbor < 
% filter_neighbordistance_bl bodylength. 
% The remaining input parameters are:
% trajectories - A #frames X #individuals X 2 array containing the
% x-y-positions of each individual for each frame.
% bodylength - Scalar value containing the average bodylength of individuals in pixels
% framerate - Framerate of the source video in frames per second.

smooth_method = 'moving'; % Method used for trajectory smoothing: Moving average. 
smooth_degree =30; % Window size for the moving average smoothing..

no_frames = size(trajectories,1);
no_fish = size(trajectories,2);
no_dim = size(trajectories,3);

% Smooth trajectories
tr_temp = NaN(size(trajectories));
for ff=1:no_fish
    for d=1:no_dim
        tr_temp(:,ff,d)=smooth(trajectories(:,ff,d),smooth_degree,smooth_method);
    end
end
trajectories = tr_temp;
% End: Smoothing

% Calculate speed
vel=NaN(size(trajectories));
vel(1:no_frames-1,:,:)=diff(trajectories,1,1);
vel_magn=sqrt(nansum(vel.^2,3));
vel_norm=bsxfun(@rdivide,vel,vel_magn);
% End: Calculate velocity

% Calculate the angle between focal movement direction and neighbor
% position
foc_to_nb_vec = NaN(no_frames,no_fish,no_fish,no_dim);
nbAng = NaN(no_frames,no_fish,no_fish);
for ff = 1:no_fish
    for nf = 1:no_fish
        if ff~=nf
            foc_to_nb_vec(:,ff,nf,:)=squeeze(trajectories(:,nf,:)-trajectories(:,ff,:));
            nbDir = squeeze(foc_to_nb_vec(:,ff,nf,:));
            nbDir = nbDir./repmat(sqrt(sum(nbDir.^2,2)),[1 2]);
            nbAng(:,ff,nf) =...
                atan2(vel_norm(:,ff,1).*nbDir(:,2)-nbDir(:,1).*vel_norm(:,ff,2), ...
                vel_norm(:,ff,1).*nbDir(:,1)+vel_norm(:,ff,2).*nbDir(:,2));
        end
    end
end

% Distance focal - neighbor
foc_to_nb_distance = sqrt(sum(foc_to_nb_vec.^2,4))/bodylength;

% Find attack frames 
aggressionFrames = NaN(no_frames,no_fish,no_fish);
for ff = 1:no_fish
    for nf  = 1:no_fish
        if ff ~=nf
            frontal_filter = abs(nbAng(:,ff,nf)) < filter_neighborfrontangle;
            speed_filter = vel_magn(:,ff)/bodylength*framerate > filter_focalspeed_bl_s;
            distance_filter = foc_to_nb_distance(:,ff,nf) < filter_neighbordistance_bl;
            aggressionFrames(:,ff,nf) = frontal_filter & speed_filter & distance_filter;
        end
    end
end
