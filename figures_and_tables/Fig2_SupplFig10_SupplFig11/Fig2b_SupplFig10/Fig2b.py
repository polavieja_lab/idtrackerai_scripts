import os
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import glob
from scipy.ndimage.filters import convolve1d
import seaborn as sns


def plot_main_figure(data_arrays, save_folder):
    ### Plotting setup
    sns.set_style('ticks')
    plt.ion()
    fig, ax_arr = plt.subplots(3,1, figsize = (6,6), sharex = True, sharey = True)
    plt.subplots_adjust(left=.1, bottom=.25, right=.9, top=.75,
                wspace=.1, hspace=.1)

    myorder=[0,1,2]
    print(len(data_arrays))
    data_arrays = [data_arrays[i] for i in myorder]
    for i, data_array in enumerate(data_arrays):
        ax = ax_arr[i]
        ax.plot(np.arange(data_array.shape[0])/30./60., data_array[:,0])
        ax.plot(np.arange(data_array.shape[0])/30./60., data_array[:,1])
        sns.despine(ax = ax, top = True, right = True)

        if i == len(myorder)-1:
            ax.set_xlabel('Time (min)', fontsize=16)
        if i == 1:
            ax.set_ylabel('Attack score', fontsize=16)

    fig.savefig(os.path.join(save_folder, 'Fig2b.pdf'))


def read_data(data_folder):
    data_files = glob.glob(os.path.join(data_folder, '*.csv'))
    print(data_files)
    data_arrays = []
    for data_file in data_files:
        data_arrays.append(np.genfromtxt(data_file, delimiter=','))
    return data_arrays


def main(csv_attack_score_folder):
    data_arrays = read_data(csv_attack_score_folder)
    plot_main_figure(data_arrays, os.path.dirname(os.path.dirname(csv_attack_score_folder)))


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description=
        """This script plots the time series of the attacks scores shown in fig2b.
        The plots are generated from csv files with the attacks scores stored in the
        'Fig2b_data/attacks_scores'. These csv files can be generated from the script SupplFig10.m""")
    parser.add_argument('-asfp', '--attack_scores_folder_path', type=str, default=None,
                        help='path to the "attacks_score folder"')
    args = parser.parse_args()
    csv_attack_score_folder = args.attack_scores_folder_path
    main(csv_attack_score_folder)
