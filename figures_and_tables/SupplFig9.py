from __future__ import absolute_import, print_function, division
import os
import numpy as np
import matplotlib.pyplot as plt
plt.ion()


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description=
        """This script generates the Supplementary Figure 9.
        The plot is generated from the video_object.npy of two videos with
        different light conditions tracked with idtracker.ai. The files to produce
        the figure can be found in the folder 'idtrackerai_figures_and_tables_data/SupplFig9_data/inhomogeneous_light_conditions'.
        The videos, the results of the tracking and the tracking software can be downloaded from idtracker.ai
        or from the corresponding author upon request.""")
    parser.add_argument("-ilcp", "--inhomogeneous_light_conditions_path", type=str,
                        help="path to the inhomogeneous_light_conditions folder")
    args = parser.parse_args()
    inhomogeneous_video_path = 'inhomogeneous_light_60fish/session_20180713/video_object.npy'
    inhomogeneous_video_path = os.path.join(args.inhomogeneous_light_conditions_path,
                                            inhomogeneous_video_path)
    homogeneous_video_path = 'homogeneous_light_60fish/session_20180714/video_object.npy'
    homogeneous_video_path = os.path.join(args.inhomogeneous_light_conditions_path,
                                          homogeneous_video_path)
    inhomogeneous_video = np.load(inhomogeneous_video_path).item()
    homogeneous_video = np.load(homogeneous_video_path).item()

    i_bkg = inhomogeneous_video.bkg
    h_bkg = homogeneous_video.bkg
    print('i_bkg min max: ', i_bkg.min(), i_bkg.max())
    print('h_bkg min max: ', h_bkg.min(), h_bkg.max())
    i_ROI = np.clip(inhomogeneous_video.ROI, 0, 1)
    h_ROI = np.clip(homogeneous_video.ROI, 0, 1)
    min_intensity = np.min([h_bkg.min(), i_bkg.min()])
    max_intensity = np.max([h_bkg.max(), i_bkg.max()])
    fig, ax_arr = plt.subplots(1, 2, figsize = (10, 5))
    ax_arr[0].imshow(h_bkg*h_ROI, cmap='jet', interpolation='nearest', vmin=min_intensity, vmax=max_intensity)
    ax_arr[0].set_title('Standard light conditions \n(all lights on)', fontsize=16)
    ax_arr[0].set_xticks([]), ax_arr[0].set_yticks([])
    ax_arr[0].set_xlabel(r'Accuracy = {:.4%}'.format(homogeneous_video.gt_accuracy['accuracy']), fontsize=14)
    ax_arr[1].set_xticks([]), ax_arr[1].set_yticks([])
    ax_arr[1].imshow(i_bkg*i_ROI, cmap='jet', interpolation='nearest', vmin=min_intensity, vmax=max_intensity)
    ax_arr[1].set_title('Manipulated light conditions \n(bottom and right lights off)', fontsize=16)
    ax_arr[1].set_xlabel(r'Accuracy = {:.4%}'.format(inhomogeneous_video.gt_accuracy['accuracy']), fontsize=14)
    plt.show()

    fig.savefig(os.path.join(os.path.dirname(os.path.dirname(args.inhomogeneous_light_conditions_path)), 'SupplFig_9.pdf'))
