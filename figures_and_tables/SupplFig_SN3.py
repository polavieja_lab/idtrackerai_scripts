from __future__ import absolute_import, division, print_function
import os
import sys
import cv2
import numpy as np
# This line is for retro-compatibility of the video tracked with the version
# of idtracker.ai that still was not a python package.
import idtrackerai
[sys.path.append(x[0]) for x in os.walk(os.path.dirname(idtrackerai.__file__))]


def put_pixels_in_frame(frame, blob, color):
    pxs = np.array(np.unravel_index(blob.pixels,(video.height, video.width))).T
    if color == 'r':
        frame[pxs[:,0], pxs[:,1], 1] -= 50
        frame[pxs[:,0], pxs[:,1], 2] -= 50
    elif color == 'g':
        frame[pxs[:,0], pxs[:,1], 0] -= 50
        frame[pxs[:,0], pxs[:,1], 2] -= 50
    elif color == 'k':
        frame[pxs[:,0], pxs[:,1], :] -= 50

    return frame


def extract_image(frames_range, image_name, list_of_blobs, save_folder):
    frame = np.ones((video.height, video.width, 3)) * 255

    for frame_number in range(frames_range[0], frames_range[1], 2):
        print(frame_number)
        blobs_in_frame = list_of_blobs.blobs_in_video[frame_number]
        for blob in blobs_in_frame:
            if isinstance(blob.assigned_identity, int):
                if blob.assigned_identity == 1:
                    frame = put_pixels_in_frame(frame, blob, 'r')
                elif blob.assigned_identity == 4:
                    frame = put_pixels_in_frame(frame, blob, 'g')
            elif isinstance(blob.assigned_identity, list) and (1 in blob.assigned_identity or 4 in blob.assigned_identity):
                frame = put_pixels_in_frame(frame, blob, 'k')

    cv2.imwrite(os.path.join(save_folder, image_name), frame)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description=
         """This scripts generates the individual fragments and the crossing
         fragment sketch of Supplementary Figure SN3. It requires the blobs_collection_no_gaps.npy
         and the video_object.npy from the example video tracked with the default values. The results
         of this tracking can be found in 'idtracker.ai_figures_and_tables_data/idtracker.ai_figures_and_tables_data/idtracker.ai_figures_and_tables_data'""")
    parser.add_argument("-sf", "--session_folder", type=str,
                        help="path to the session_test folder")
    args = parser.parse_args()
    save_folder = os.path.dirname(os.path.dirname(args.session_folder))
    session_folder = args.session_folder
    path_to_list_of_blobs = os.path.join(session_folder, 'preprocessing/blobs_collection_no_gaps.npy')
    path_to_video_object = os.path.join(session_folder, 'video_object.npy')
    video = np.load(path_to_video_object).item()
    list_of_blobs = np.load(path_to_list_of_blobs).item()
    print("plottig frame")
    individual_fragments = (44, 50)
    frames_all_crossing = (44, 56)
    crossing_fragment = (50, 56)
    extract_image(individual_fragments, 'SupplFigSN3_before_crossing.png', list_of_blobs, save_folder)
    extract_image(frames_all_crossing, 'SupplFigSN3_in_crossing.png', list_of_blobs, save_folder)
    extract_image(crossing_fragment, 'SupplFigSN3_after_crossing.png', list_of_blobs, save_folder)
