from __future__ import absolute_import, division, print_function
import os
import sys
import numpy as np
import datetime
from pprint import pprint
import pandas as pd
from glob import glob


def get_condition_folders(hard_drive_path):
    return [f for f in glob(os.path.join(hard_drive_path, '*'))
            if os.path.isdir(f)]

def get_video_folders(condition_folder):
    return [f for f in glob(os.path.join(condition_folder, '*'))
            if os.path.isdir(f)]


def get_session_folders(video_folder):
    if 'compression' not in condition_folder:
        session_folders = [f for f in glob(os.path.join(video_folder,
                                                        '*'))
                           if os.path.isdir(f)]
    else:
        compression_type_folders = [f for f in glob(os.path.join(
            video_folder, '*')) if os.path.isdir(f)]
        session_folders = [sf for f in compression_type_folders
                           for sf in glob(os.path.join(f, '*'))
                           if os.path.isdir(sf)]
    return session_folders


def compute_protocol(video):
    if not video.has_been_pretrained and len(video.validation_accuracy) == 1:
        video.protocol = 1
    elif not video.has_been_pretrained and len(video.validation_accuracy) >= 2:
        video.protocol = 2
    elif video.has_been_pretrained:
        video.protocol = 3


def compute_protocol_cascade_time(video):
    if hasattr(video, 'protocol1_time'):
        video.protocol_cascade_time = \
            video.protocol1_time + video.protocol2_time +\
            video.protocol3_pretraining_time + video.protocol3_accumulation_time
    elif 'flies' in video.session_folder:
        video.protocol_cascade_time = 216538


def compute_percentage_of_images_identified(video):
    if video.tracking_with_knowledge_transfer:
        gt_results = video.gt_results
    else:
        gt_results = video.gt_results_interpolated
    video.percentage_of_individual_blobs_identified = \
        np.sum(gt_results['number_of_assigned_blobs_per_identity'].values())/gt_results['number_of_individual_blobs']


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description=
        """This script generates the dataframe with all the information about
        the videos tracked with idtracker.ai in the Supplementary
        Table 11. The raw videos, the results of the tracking, the tracking sowftware
        can be downloaded from idtracker.ai or from the corresponding author of the paper.
        The content of the hard drive transfer_learning_tests can also be downloaded from idtracker.ai.
        """)
    parser.add_argument("-hdp", "--hard_drive_path", type=str,
                        help="path to the transfer_leraning_tests \
                        hard drive")
    args = parser.parse_args()

    animals_folders = glob(os.path.join(args.hard_drive_path,'*'))

    videos_folders = []
    for animals_folder in animals_folders:
        video_folders = [f for f in glob(os.path.join(animals_folder, '*'))
                         if 'transfer' not in os.path.split(f)[-1]]
        videos_folders.extend(video_folders)

    sessions_folders = []
    for video_folder in videos_folders:
        session_folders = [f for f in glob(os.path.join(video_folder, '*'))
                           if ('kt_freezing' in f or f[-1] is not 't') and os.path.isdir(f)]
        sessions_folders.extend(session_folders)

    pprint(sessions_folders)

    transfer_learning_data_frame = pd.DataFrame()

    for session_folder in sessions_folders:
        print("\nsession_folder ", session_folder)
        video_object_path = os.path.join(session_folder,
                                         'video_object.npy')
        video = np.load(video_object_path).item()
        video.update_paths(video_object_path)
        if not hasattr(video, 'protocol'):
            compute_protocol(video)

        # if not hasattr(video, 'tracking_time'):
        compute_protocol_cascade_time(video)

        # compute percentage of images identified
        compute_percentage_of_images_identified(video)

        video.save()
        transfer_learning_data_frame = \
            transfer_learning_data_frame.append({
                'video title': 'to be filled',
                'knowledge_tranfer_folder': video._accumulation_network_params.knowledge_transfer_folder if video.tracking_with_knowledge_transfer else 'no kt',
                'knowledge_transfer': video.tracking_with_knowledge_transfer,
                'git_commit': video.git_commit,
                'protocol_cascade_time_sec': video.protocol_cascade_time,
                'protocol_cascade_time': str(datetime.timedelta(seconds=int(video.protocol_cascade_time))),
                'video_title': None,
                'video_name': os.path.split(video.video_path)[1],
                'animal_type': None,
                'idTracker_video': None,
                'number_of_animals': video.number_of_animals,
                'number_of_frames': video.number_of_frames,
                'frame_rate': video.frames_per_second,
                'min_threshold': video.min_threshold,
                'max_threshold': video.max_threshold,
                'min_area': video.min_area,
                'max_area': video.max_area,
                'subtract_bkg': video.subtract_bkg,
                'apply_ROI': video.apply_ROI,
                'resolution_reduction': video.resolution_reduction,
                'resegmentation_parameters': video.resegmentation_parameters,
                'maximum_number_of_blobs': video.maximum_number_of_blobs,
                'width': video.width,
                'height': video.height,
                'original_width': video.original_width,
                'original_height': video.original_height,
                'video_length_sec': video.number_of_frames/video.frames_per_second,
                'mean_area_in_pixels': video.model_area.mean,
                'std_area_in_pixels': video.model_area.std,
                'body_length': video.median_body_length,
                'identification_image_size': video.identification_image_size[0],
                'protocol': video.protocol,
                'accumulation_trial': video.accumulation_trial,
                'number_of_accumulation_steps': len(video.validation_accuracy),
                'percentage_of_accumulated_images': video.percentage_of_accumulated_images[video.accumulation_trial],
                'estimated_accuracy': video.overall_P2,
                'interval_of_frames_validated': video.gt_start_end,
                'number_of_frames_validated': np.diff(video.gt_start_end)[0],
                'percentage_of_video_validated': np.diff(video.gt_start_end)[0]/video.number_of_frames*100,
                'time_validated_min': np.diff(video.gt_start_end)[0]/video.frames_per_second/60,
                'number_of_crossing_fragments_in_validated_part': video.gt_results['number_of_crossing_fragments'],
                'number_of_crossing_images_in_validated_part': video.gt_results['number_of_crossing_blobs'],
                'percentage_of_unoccluded_images': video.gt_accuracy['percentage_of_unoccluded_images'],
                'estimated_accuracy_in_validated_part': video.gt_accuracy['mean_individual_P2_in_validated_part'],
                'accuracy_in_accumulation': video.gt_accuracy['accuracy_in_accumulation'],
                'accuracy': video.gt_accuracy['accuracy'] if video.tracking_with_knowledge_transfer else video.gt_accuracy_interpolated['accuracy'],
                'accuracy_identified_animals': video.gt_accuracy['accuracy_assigned'] if video.tracking_with_knowledge_transfer else video.gt_accuracy_interpolated['accuracy_assigned'],
                'accuracy_in_residual_identification': video.gt_accuracy['accuracy_after_accumulation'],
                'percentage_of_individual_blobs_identified': video.percentage_of_individual_blobs_identified,
                }, ignore_index=True)


    transfer_learning_data_frame.to_pickle(os.path.join(
        args.hard_drive_path, 'trasnfer_learning_data_frame.pkl'))
