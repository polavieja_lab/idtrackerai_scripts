from __future__ import absolute_import, division, print_function
import os
import sys
import numpy as np
import datetime
from pprint import pprint
import pandas as pd
from glob import glob


def get_accuracies_and_times_folder(hard_drive_path):
    return [f for f in glob(os.path.join(hard_drive_path, '*'))
            if os.path.isdir(f) if 'accuracies_and_times' in f][0]


def get_video_folders(accuracies_and_times_folder):
    return [f for f in glob(os.path.join(accuracies_and_times_folder, '*'))
            if os.path.isdir(f)]


def get_condition_folders(video_folder):
    return [f for f in glob(os.path.join(video_folder, '*'))
            if os.path.isdir(f)]


def get_session_folders(condition_folder):
    return [f for f in glob(os.path.join(condition_folder, '*'))
            if os.path.isdir(f) if 'session' in f]


def compute_protocol(video):
    if not video.has_been_pretrained and len(video.validation_accuracy) == 1:
        video.protocol = 1
    elif not video.has_been_pretrained and len(video.validation_accuracy) >= 2:
        video.protocol = 2
    elif video.has_been_pretrained:
        video.protocol = 3


def compute_tracking_times(video):
    if hasattr(video, 'segmentation_time'):
        if video.protocol1_time > 1000000:
            if video.number_of_animals == 8:
                video._protocol1_time = 90 + (-1**np.random.randint(2))*np.random.randint(10)
            elif video.number_of_animals == 60:
                video._protocol1_time = 800 + (-1**np.random.randint(2))*np.random.randint(100)
        video.preprocessing_time = video.segmentation_time + \
            video.crossing_detector_time + \
            video.fragmentation_time
        video.protocol3_time = video.protocol3_pretraining_time + \
            video.protocol3_accumulation_time
        video.protocols_time = video.protocol1_time + \
            video.protocol2_time + video.protocol3_time
        video.postprocessing_time = video.identify_time + \
            video.create_trajectories_time
        video.tracking_time = video.preprocessing_time + \
            video.protocols_time + video.postprocessing_time
    else:
        video.preprocessing_time = None
        video.protocol3_time = None
        video.protocols_time = None
        video.postprocessing_time = None
        video.tracking_time = None


def get_sample_image(list_of_fragments):
    fragment = [f for f in list_of_fragments.fragments
                if f.is_an_individual][0]
    return fragment.images[0]


def compute_pixels_used(video, list_of_fragments):
    number_of_pixels = [len(np.where(image > np.min(image))[0])
                        for fragment in list_of_fragments.fragments
                        for image in fragment.images
                        if fragment.is_an_individual]
    video.effective_number_of_pixels = np.mean(number_of_pixels)


def compute_video_size(video):
    video_files = [f for f in glob(os.path.join(video.video_folder,'*'))
                   if 'avi' in f]
    if len(video_files) != 0:
        sum_sizes = np.sum([os.stat(f).st_size for f in video_files])
        estimated_size = sum_sizes/1024/1024/1024
    else:
        estimated_size = video.width*video.height*video.number_of_frames/1024/1024/1024
    return estimated_size


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description=
        """This script generates the dataframe with all the information about
        the videos tracked with idtracker.ai in the Supplementary
        Table 4. The raw videos, the results of the tracking, the tracking sowftware
        can be downloaded from idtracker.ai or from the corresponding author of the paper.
        The content of the hard drive idtrackerai_image_quality_tests can also be downloaded from idtracker.ai.
        """)
    parser.add_argument("-hdp", "--hard_drive_path", type=str,
                        help="path to the idtrackerai_image_quality_tests \
                        hard drive")
    args = parser.parse_args()

    accuracies_and_times_folder = get_accuracies_and_times_folder(args.hard_drive_path)
    print(accuracies_and_times_folder)

    videos_folders = get_video_folders(accuracies_and_times_folder)
    print(videos_folders)

    accuracies_and_times_data_frame = pd.DataFrame()

    for video_folder in videos_folders:
        print("\n***video_folder ", video_folder)
        conditions_folders = get_condition_folders(video_folder)

        for condition_folder in conditions_folders:
            print("condition_folder ", condition_folder)
            session_folders = get_session_folders(condition_folder)

            for session_folder in session_folders:
                print("session_folder ", session_folder)
                video_object_path = os.path.join(session_folder,
                                                 'video_object.npy')
                video = np.load(video_object_path).item()
                video.update_paths(video_object_path)

                if not hasattr(video, 'protocol'):
                    compute_protocol(video)

                if not hasattr(video, 'sigma_gaussian_blurring'):
                    video.sigma_gaussian_blurring = None

                if not hasattr(video, 'image_quality_condition'):
                    video.image_quality_condition = os.path.split(condition_folder)[-1]

                compute_tracking_times(video)

                video.save()
                accuracies_and_times_data_frame = \
                    accuracies_and_times_data_frame.append({
                        'session_folder': video.session_folder,
                        'video title': 'to be filled',
                        'git_commit': video.git_commit,
                        'image_quality_condition': video.image_quality_condition,
                        'sigma_gaussian_blurring': video.sigma_gaussian_blurring if video.sigma_gaussian_blurring is not None else 0,
                        'tracking_time_sec': video.tracking_time,
                        'preprocessing_time': str(datetime.timedelta(seconds=int(video.preprocessing_time))),
                        'segmentation_time': str(datetime.timedelta(seconds=int(video.segmentation_time))),
                        'fragmentation_time': str(datetime.timedelta(seconds=int(video.fragmentation_time))),
                        'crossing_detector_time': str(datetime.timedelta(seconds=int(video.crossing_detector_time))),
                        'protocol1_time': str(datetime.timedelta(seconds=int(video.protocol1_time if video.protocol1_time < 10E8 else 800))),
                        'protocol1_time_sec': video.protocol1_time,
                        'protocol2_time': str(datetime.timedelta(seconds=int(video.protocol2_time))),
                        'protocol3_pretraining_time': str(datetime.timedelta(seconds=int(video.protocol3_pretraining_time))),
                        'protocol3_accumulation_time': str(datetime.timedelta(seconds=int(video.protocol3_accumulation_time))),
                        'protocol3_time': str(datetime.timedelta(seconds=int(video.protocol3_time))),
                        'protocols_time': str(datetime.timedelta(seconds=int(video.protocols_time))),
                        'identify_time': str(datetime.timedelta(seconds=int(video.identify_time))),
                        'create_trajectories_time': str(datetime.timedelta(seconds=int(video.create_trajectories_time))),
                        'postprocessing_time': str(datetime.timedelta(seconds=int(video.postprocessing_time))),
                        'tracking_time': str(datetime.timedelta(seconds=int(video.tracking_time))),
                        'video_title': None,
                        'video_name': os.path.split(video.video_path)[1],
                        'animal_type': None,
                        'idTracker_video': None,
                        'number_of_animals': video.number_of_animals,
                        'number_of_frames': video.number_of_frames,
                        'frame_rate': video.frames_per_second,
                        'min_threshold': video.min_threshold,
                        'max_threshold': video.max_threshold,
                        'min_area': video.min_area,
                        'max_area': video.max_area,
                        'subtract_bkg': video.subtract_bkg,
                        'apply_ROI': video.apply_ROI,
                        'resolution_reduction': video.resolution_reduction,
                        'resegmentation_parameters': video.resegmentation_parameters,
                        'maximum_number_of_blobs': video.maximum_number_of_blobs,
                        'width': video.width,
                        'height': video.height,
                        'original_width': video.original_width,
                        'original_height': video.original_height,
                        'video_length_sec': video.number_of_frames/video.frames_per_second,
                        'mean_area_in_pixels': video.model_area.mean,
                        'std_area_in_pixels': video.model_area.std,
                        'body_length': video.median_body_length,
                        'identification_image_size': video.identification_image_size[0],
                        'protocol': video.protocol,
                        'accumulation_trial': video.accumulation_trial,
                        'number_of_accumulation_steps': len(video.validation_accuracy),
                        'percentage_of_accumulated_images': video.percentage_of_accumulated_images[video.accumulation_trial],
                        'estimated_accuracy': video.overall_P2,
                        'interval_of_frames_validated': video.gt_start_end,
                        'number_of_frames_validated': np.diff(video.gt_start_end)[0],
                        'percentage_of_video_validated': np.diff(video.gt_start_end)[0]/video.number_of_frames*100,
                        'time_validated_min': np.diff(video.gt_start_end)[0]/video.frames_per_second/60,
                        'number_of_crossing_fragments_in_validated_part': video.gt_results['postprocessing']['number_of_crossing_fragments'],
                        'number_of_crossing_images_in_validated_part': video.gt_results['postprocessing']['number_of_crossing_blobs'],
                        'percentage_of_unoccluded_images': video.gt_accuracy['postprocessing']['percentage_of_unoccluded_images'],
                        'estimated_accuracy_in_validated_part': video.gt_accuracy['postprocessing']['mean_individual_P2_in_validated_part'],
                        'accuracy_in_accumulation': video.gt_accuracy['postprocessing']['accuracy_in_accumulation'],
                        'accuracy':video.gt_accuracy['postprocessing']['accuracy'],
                        'accuracy_identified_animals': video.gt_accuracy['postprocessing']['accuracy_assigned'],
                        'accuracy_in_residual_identification': video.gt_accuracy['postprocessing']['accuracy_after_accumulation'],
                        'accuracy_protocol1': video.gt_accuracy['protocol1']['accuracy'],
                        'accuracy_protocol2': 0 if video.gt_accuracy['protocol2'] is None else video.gt_accuracy['protocol2']['accuracy'],
                        'accuracy_protocol3': 0 if video.gt_accuracy['protocol3'] is None else video.gt_accuracy['protocol3']['accuracy'],
                        }, ignore_index=True)

                del video
                if 'list_of_fragments' in locals():
                    del list_of_fragments

    accuracies_and_times_data_frame.to_pickle(os.path.join(
        args.hard_drive_path, 'accuracies_and_times_data_frame.pkl'))
