from __future__ import absolute_import, division, print_function
import os
import sys
import numpy as np
import datetime
from pprint import pprint
import pandas as pd
from glob import glob


def get_condition_folders(hard_drive_path):
    return [f for f in glob(os.path.join(hard_drive_path, '*'))
            if os.path.isdir(f)]

def get_video_folders(condition_folder):
    return [f for f in glob(os.path.join(condition_folder, '*'))
            if os.path.isdir(f)]


def get_session_folders(video_folder):
    if 'compression' not in condition_folder:
        session_folders = [f for f in glob(os.path.join(video_folder,
                                                        '*'))
                           if os.path.isdir(f)]
    else:
        compression_type_folders = [f for f in glob(os.path.join(
            video_folder, '*')) if os.path.isdir(f)]
        session_folders = [sf for f in compression_type_folders
                           for sf in glob(os.path.join(f, '*'))
                           if os.path.isdir(sf)]
    return session_folders


def compute_protocol(video):
    if not video.has_been_pretrained and len(video.validation_accuracy) == 1:
        video.protocol = 1
    elif not video.has_been_pretrained and len(video.validation_accuracy) >= 2:
        video.protocol = 2
    elif video.has_been_pretrained:
        video.protocol = 3


def get_compression(video, session_folder, condition_folder):
    if 'compression' in condition_folder:
        compression_folder = os.path.split(session_folder)[0]
        compression_str = os.path.split(compression_folder)[-1]
        if compression_str == 'h264_crf0':
            video.compression = 'H264'
        elif compression_str == 'mpeg4_q1':
            video.compression = 'MPEG4'
    else:
        video.compression = 'raw video'


def compute_tracking_time(video):
    if hasattr(video, 'segmentation_time'):
        protocol1_time = video.protocol1_time if video.protocol1_time < 10E8 else 800
        video.tracking_time = video.segmentation_time + \
            video.crossing_detector_time + \
            video.fragmentation_time + protocol1_time + \
            video.protocol2_time + video.protocol3_pretraining_time + \
            video.protocol3_accumulation_time + video.identify_time + \
            video.create_trajectories_time
    else:
        video.tracking_time = None


def get_sample_image(list_of_fragments):
    fragment = [f for f in list_of_fragments.fragments
                if f.is_an_individual][0]
    return fragment.images[0]


def compute_pixels_used(video, list_of_fragments):
    number_of_pixels = [len(np.where(image > np.min(image))[0])
                        for fragment in list_of_fragments.fragments
                        for image in fragment.images
                        if fragment.is_an_individual]
    video.effective_number_of_pixels = np.mean(number_of_pixels)

def compute_video_size(video):
    video_files = [f for f in glob(os.path.join(video.video_folder,'*'))
                   if 'avi' in f]
    if len(video_files) != 0:
        sum_sizes = np.sum([os.stat(f).st_size for f in video_files])
        estimated_size = sum_sizes/1024/1024/1024
    else:
        estimated_size = video.width*video.height*video.number_of_frames/1024/1024/1024
    return estimated_size

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description=
        """This script generates the dataframe with all the information about
        the videos tracked with idtracker.ai in the Supplementary
        Table 8 9 and 10. The raw videos, the results of the tracking, the tracking sowftware
        can be downloaded from idtracker.ai or from the corresponding author of the paper.
        The content of the hard drive idtrackerai_image_quality_tests can also be downloaded from idtracker.ai.
        """)
    parser.add_argument("-hdp", "--hard_drive_path", type=str,
                        help="path to the idtrackerai_image_quality_tests \
                        hard drive")
    args = parser.parse_args()

    condition_folders = get_condition_folders(args.hard_drive_path)

    image_quality_data_frame = pd.DataFrame()

    for condition_folder in condition_folders:
        print("\ncondition_folder ", condition_folder)
        videos_folders = get_video_folders(condition_folder)

        for video_folder in videos_folders:
            print("video_folder ", video_folder)
            session_folders = get_session_folders(video_folder)

            for session_folder in session_folders:
                print("session_folder ", session_folder)
                video_object_path = os.path.join(session_folder,
                                                 'video_object.npy')
                video = np.load(video_object_path).item()
                video.update_paths(video_object_path)

                if not hasattr(video, 'protocol'):
                    compute_protocol(video)

                # if not hasattr(video, 'compression'):
                get_compression(video, session_folder, condition_folder)

                if not hasattr(video, 'sigma_gaussian_blurring'):
                    video.sigma_gaussian_blurring = None

                if not hasattr(video, 'image_quality_condition'):
                    video.image_quality_condition = os.path.split(condition_folder)[-1]

                video.video_size_Gbytes = compute_video_size(video)

                # if not hasattr(video, 'tracking_time'):
                compute_tracking_time(video)

                if not hasattr(video, 'sample_image'):
                    list_of_fragments = np.load(video.fragments_path).item()
                    video.sample_image = get_sample_image(list_of_fragments)

                if not hasattr(video, 'effective_number_of_pixels'):
                    if 'list_of_fragments' not in locals():
                        list_of_fragments = np.load(video.fragments_path).item()
                    compute_pixels_used(video, list_of_fragments)

                video.save()
                image_quality_data_frame = \
                    image_quality_data_frame.append({
                        'video title': 'to be filled',
                        'size': video.video_size_Gbytes,
                        'git_commit': video.git_commit,
                        'image_quality_condition': video.image_quality_condition,
                        'sigma_gaussian_blurring': video.sigma_gaussian_blurring if video.sigma_gaussian_blurring is not None else 0,
                        'tracking_time_sec': video.tracking_time,
                        'segmentation_time': str(datetime.timedelta(seconds=int(video.segmentation_time))),
                        'fragmentation_time': str(datetime.timedelta(seconds=int(video.fragmentation_time))),
                        'crossing_detector_time': str(datetime.timedelta(seconds=int(video.crossing_detector_time))),
                        'protocol1_time': str(datetime.timedelta(seconds=int(video.protocol1_time if video.protocol1_time < 10E8 else 800))),
                        'protocol2_time': str(datetime.timedelta(seconds=int(video.protocol2_time))),
                        'protocol3_pretraining_time': str(datetime.timedelta(seconds=int(video.protocol3_pretraining_time))),
                        'protocol3_accumulation_time': str(datetime.timedelta(seconds=int(video.protocol3_accumulation_time))),
                        'identify_time': str(datetime.timedelta(seconds=int(video.identify_time))),
                        'create_trajectories_time': str(datetime.timedelta(seconds=int(video.create_trajectories_time))),
                        'tracking_time': str(datetime.timedelta(seconds=int(video.tracking_time))),
                        'compression': video.compression,
                        'effective_number_of_pixels': video.effective_number_of_pixels,
                        'sample_image': video.sample_image,
                        'video_title': None,
                        'video_name': os.path.split(video.video_path)[1],
                        'animal_type': None,
                        'idTracker_video': None,
                        'number_of_animals': video.number_of_animals,
                        'number_of_frames': video.number_of_frames,
                        'frame_rate': video.frames_per_second,
                        'min_threshold': video.min_threshold,
                        'max_threshold': video.max_threshold,
                        'min_area': video.min_area,
                        'max_area': video.max_area,
                        'subtract_bkg': video.subtract_bkg,
                        'apply_ROI': video.apply_ROI,
                        'resolution_reduction': video.resolution_reduction,
                        'resegmentation_parameters': video.resegmentation_parameters,
                        'maximum_number_of_blobs': video.maximum_number_of_blobs,
                        'width': video.width,
                        'height': video.height,
                        'original_width': video.original_width,
                        'original_height': video.original_height,
                        'video_length_sec': video.number_of_frames/video.frames_per_second,
                        'mean_area_in_pixels': video.model_area.mean,
                        'std_area_in_pixels': video.model_area.std,
                        'body_length': video.median_body_length,
                        'identification_image_size': video.identification_image_size[0],
                        'protocol': video.protocol,
                        'accumulation_trial': video.accumulation_trial,
                        'number_of_accumulation_steps': len(video.validation_accuracy),
                        'percentage_of_accumulated_images': video.percentage_of_accumulated_images[video.accumulation_trial],
                        'estimated_accuracy': video.overall_P2,
                        'interval_of_frames_validated': video.gt_start_end,
                        'number_of_frames_validated': np.diff(video.gt_start_end)[0],
                        'percentage_of_video_validated': np.diff(video.gt_start_end)[0]/video.number_of_frames*100,
                        'time_validated_min': np.diff(video.gt_start_end)[0]/video.frames_per_second/60,
                        'number_of_crossing_fragments_in_validated_part': video.gt_results['number_of_crossing_fragments'],
                        'number_of_crossing_images_in_validated_part': video.gt_results['number_of_crossing_blobs'],
                        'percentage_of_unoccluded_images': video.gt_accuracy['percentage_of_unoccluded_images'],
                        'estimated_accuracy_in_validated_part': video.gt_accuracy['mean_individual_P2_in_validated_part'],
                        'accuracy_in_accumulation': video.gt_accuracy['accuracy_in_accumulation'],
                        'accuracy':video.gt_accuracy['accuracy'],
                        'accuracy_identified_animals': video.gt_accuracy['accuracy_assigned'],
                        'accuracy_in_residual_identification': video.gt_accuracy['accuracy_after_accumulation'],
                        }, ignore_index=True)

                del video
                if 'list_of_fragments' in locals():
                    del list_of_fragments

    image_quality_data_frame.to_pickle(os.path.join(
        args.hard_drive_path, 'image_quality_data_frame.pkl'))
