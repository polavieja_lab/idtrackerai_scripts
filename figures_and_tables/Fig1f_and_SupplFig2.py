from __future__ import absolute_import, division, print_function
import os
import sys
import numpy as np
import collections
from matplotlib import pyplot as plt
import matplotlib
import seaborn as sns
import pandas as pd
# This line is for retro-compatibility of the video tracked with the version
# of idtracker.ai that still was not a python package.
import idtrackerai
[sys.path.append(x[0]) for x in os.walk(os.path.dirname(idtrackerai.__file__))]

MARKERS = matplotlib.markers.MarkerStyle.markers.keys()[5:]


def flatten(l):
    for el in l:
        if isinstance(el, collections.Iterable) and not isinstance(el, basestring):
            for sub in flatten(el):
                yield sub
        else:
            yield el


def get_repetition_averaged_data_frame(results_data_frame):
    repetition_averaged_data_frame = pd.DataFrame(columns = [results_data_frame.mean().to_dict().keys()])
    count = 0

    for group_size in results_data_frame['group_size'].unique():

        for frames_in_video in results_data_frame['frames_in_video'].unique():

            for frames_in_fragment in results_data_frame['frames_per_fragment'].unique():

                temp_data_frame = results_data_frame.query('group_size == @group_size' +
                                                            ' & frames_in_video == @frames_in_video' +
                                                            ' & frames_per_fragment == @frames_in_fragment')
                temp_dict = temp_data_frame.mean().to_dict()
                repetition_averaged_data_frame.loc[count,'test_accuracy'] = temp_dict['test_accuracy']
                repetition_averaged_data_frame.loc[count,'group_size'] = temp_dict['group_size']

                count += 1
    return repetition_averaged_data_frame


def get_repetition_std_data_frame(results_data_frame):
    repetition_std_data_frame = pd.DataFrame(columns = [results_data_frame.std().to_dict().keys()])
    count = 0

    for group_size in results_data_frame['group_size'].unique():

        for frames_in_video in results_data_frame['frames_in_video'].unique():

            for frames_in_fragment in results_data_frame['frames_per_fragment'].unique():

                temp_data_frame = results_data_frame.query('group_size == @group_size' +
                                                            ' & frames_in_video == @frames_in_video' +
                                                            ' & frames_per_fragment == @frames_in_fragment')
                temp_dict = temp_data_frame.std().to_dict()
                repetition_std_data_frame.loc[count,'test_accuracy'] = temp_dict['test_accuracy']

                count += 1
    return repetition_std_data_frame


def convolutional_modifications_axes_settings(ax_arr, legend_order_conv, results_data_frame, repetition_averaged_data_frame):
    ax = ax_arr[0]
    ax.set_title('Convolutional modifications', fontsize = 20)
    ax.set_ylabel('accuracy',fontsize = 20)
    handles, labels = ax.get_legend_handles_labels()
    handles_ordered = []
    for label_ordered in legend_order_conv:
        index = labels.index(label_ordered)
        handles_ordered.append(handles[index])
    h_legend = ax.legend(handles_ordered, legend_order_conv, loc = 3, prop={'size': 13})
    ax.set_xticks([2, 5, 10, 30, 60, 80, 100, 150])
    ax.set_xticklabels([2, '', 10, 30, 60, 80, 100, 150])
    ax.set_xlabel('Group size', fontsize = 20)
    ax.set_ylabel('Single image accuracy (test)', fontsize = 20)
    ax.set_ylim([82,100])
    ax.set_xlim([0.,np.max(repetition_averaged_data_frame['group_size'].values)+2])
    ax.tick_params(axis='both', which='major', labelsize=14)
    sns.despine(ax = ax, right = True, top = True)


def fc_modifications_axes_settings(ax_arr, legend_order_conv, results_data_frame, repetition_averaged_data_frame):
    ax = ax_arr[1]
    ax.set_title('Classification modifications', fontsize = 20)
    ax.set_yticklabels([])
    handles, labels = ax.get_legend_handles_labels()
    print('labels ', labels)
    handles_ordered = []
    for label_ordered in legend_order_fully:
        index = labels.index(label_ordered)
        handles_ordered.append(handles[index])
    h_legend = ax.legend(handles_ordered, legend_order_fully,loc = 3, prop={'size': 13})
    ax.set_xticks([2, 5, 10, 30, 60, 80, 100, 150])
    ax.set_xticklabels([2, '', 10, 30, 60, 80, 100, 150])
    ax.set_xlabel('Number of individuals to identify', fontsize = 20)
    ax.set_ylim([82,100])
    ax.set_xlim([0.,np.max(repetition_averaged_data_frame['group_size'].values)+2])
    ax.tick_params(axis='both', which='major', labelsize=14)
    sns.despine(ax = ax, right = True, top = True)


def main_fig_axes_settings(ax2,repetition_averaged_data_frame):
    h_legend = ax2.legend(loc = 4, prop={'size': 20})
    ax2.set_xticks([2, 5, 10, 30, 60, 80, 100, 150])
    ax2.set_xticklabels([2, '', 10, 30, 60, 80, 100, 150])
    ax2.set_yticks([85, 90, 95, 100])
    ax2.set_yticklabels([85, 90, 95, 100])
    ax2.set_xlabel('Number of individuals to identify', fontsize = 20)
    ax2.set_ylabel('Single image accuracy (test)', fontsize = 20)
    ax2.set_ylim([82,100])
    ax2.set_xlim([0.,np.max(repetition_averaged_data_frame['group_size'].values)+2])
    ax2.tick_params(axis='both', which='major', labelsize=20)
    sns.despine(ax = ax2, right = True, top = True)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description=
         """This script generates the Figure 1f and Supplementary Figure 2.
         The plots are generated from the results_data_frame.pkl file
         stored in 'idtrackerai_figures_and_tables_data/Fig1f_and_SupplFig2_data'
         This dataframe can be reproduced running the script 'simulations/Fig1_and_SupplFig2_experiments.py'
         Please refer to that script for more information""")
    parser.add_argument("-rdfp", "--results_data_frame_path", type=str,
                        help="path to the results_data_frame.pkl file")
    args = parser.parse_args()
    path_to_results_data_frame = args.results_data_frame_path
    ### load global results data frame
    if os.path.isfile(path_to_results_data_frame):
        print("loading results_data_frame.pkl...")
        results_data_frame = pd.read_pickle(path_to_results_data_frame)
        print("results_data_frame.pkl loaded \n")
    else:
        print("results_data_frame.pkl does not exist \n")

    # get tests_data_frame and test to plot
    print("loading tests data frame")
    tests_data_frame = pd.read_pickle(os.path.join(os.path.dirname(path_to_results_data_frame), 'tests_data_frame.pkl'))
    test_names = [test_name for test_name in results_data_frame['test_name'].unique() if 'uncorrelated' in test_name]
    new_ordered_test_names = test_names[:-3]
    new_ordered_test_names.append(test_names[-2])
    new_ordered_test_names.append(test_names[-3])
    new_ordered_test_names.append(test_names[-1])

    cnn_model_names_dict = {0: 'idtracker.ai',
                            1: '1 conv layer',
                            2: '2 conv layers',
                            3: '4 conv layers',
                            6: 'fully 50',
                            7: 'fully 200',
                            8: r'fully 100 $\rightarrow$ fully 100',
                            9: r'fully 100 $\rightarrow$ fully 50',
                            10: r'fully 100 $\rightarrow$ fully 200'}

    legend_order_conv = [cnn_model_names_dict[1],
                            cnn_model_names_dict[2],
                            '3 conv layers (idtracker.ai)',
                            cnn_model_names_dict[3]]

    legend_order_fully = [cnn_model_names_dict[6],
                            'fully 100 (idtracker.ai)',
                            cnn_model_names_dict[7],
                            cnn_model_names_dict[9],
                            cnn_model_names_dict[8],
                            cnn_model_names_dict[10]]

    idTracker_single_image_accuracy_results = np.load(os.path.join(os.path.dirname(path_to_results_data_frame), 'idTracker_results.npy'))

    # plot
    plt.ion()
    sns.set_style("ticks", {'legend.frameon':True})
    fig1, ax_arr = plt.subplots(1,2, sharex = True)
    fig1.subplots_adjust(left=None, bottom=.15, right=None, top=.9,
                wspace=None, hspace=None)
    window = plt.get_current_fig_manager().window
    screen_y = window.winfo_screenheight()
    screen_x = window.winfo_screenwidth()
    fig1.set_size_inches((screen_x*2/3/100,screen_y/1.75/100))
    fig1.canvas.set_window_title('Supplementary figure')
    # fig1.suptitle('Single image identification accuracy (MEAN) - libraries %s - %i repetitions' %('GHI',
    #                                                 len(results_data_frame['repetition'].unique())), fontsize = 25)

    fig2, ax2 = plt.subplots(1)
    fig2.set_size_inches((screen_x/3/100,screen_y/2/100))
    fig2.canvas.set_window_title('Main figure')
    fig2.subplots_adjust(left=.2, bottom=.15, right=None, top=.9,
                wspace=None, hspace=None)

    N = len(cnn_model_names_dict) - 1
    RGB_tuples = matplotlib.cm.get_cmap('jet')

    ### idTracker accuracy
    # ax_arr[0].plot(idTracker_single_image_accuracy_results * 100, 'k--', label = 'idTracker')
    # ax_arr[1].plot(idTracker_single_image_accuracy_results * 100, 'k--', label = 'idTracker')
    ax2.plot(idTracker_single_image_accuracy_results * 100, 'k--', label = 'idTracker', linewidth = 3)

    ### idTracker.ai accuracy
    test_name = test_names[0]
    results_data_frame_test = results_data_frame.query('test_name == @test_name')
    repetition_averaged_data_frame = get_repetition_averaged_data_frame(results_data_frame_test)
    repetition_std_data_frame = get_repetition_std_data_frame(results_data_frame_test)
    repetition_averaged_data_frame = repetition_averaged_data_frame.apply(pd.to_numeric, errors='ignore')
    accuracy = repetition_averaged_data_frame.test_accuracy.values
    std_accuracy = repetition_std_data_frame.test_accuracy.values
    group_sizes = repetition_averaged_data_frame.group_size.values.astype('float32')

    # plot mean accuracy
    labels_to_plot = ['3 conv layers (idtracker.ai)', 'fully 100 (idtracker.ai)', 'idtracker.ai\n' + r'(mean $\pm$ std)']
    axes_to_plot = [ax_arr[0], ax_arr[1], ax2]
    for ax, label in zip(axes_to_plot, labels_to_plot):
        if label == 'idtracker.ai\n' + r'(mean $\pm$ std)':
            (_, caps, _) = ax.errorbar(group_sizes, accuracy * 100, yerr = std_accuracy * 100, color = 'k', label = label, marker = MARKERS[0], capsize = 5, linewidth = 3)
        else:
            (_, caps, _) = ax.errorbar(group_sizes, accuracy * 100, yerr = std_accuracy * 100, color = 'k', label = label, marker = MARKERS[0], capsize = 5)
        for cap in caps:
            cap.set_markeredgewidth(1)

    # plot raw results per repetition
    for group_size in results_data_frame_test.group_size.unique():
        temp_df = results_data_frame_test.query('group_size == @group_size')
        raw_test_accuracy = temp_df.test_accuracy.values * 100
        if group_size == 2:
            ax2.scatter([group_size] * len(raw_test_accuracy), raw_test_accuracy, c = 'r', marker = 'o', alpha = .5, label = 'idtracker.ai',zorder=3)
        else:
            ax2.scatter([group_size] * len(raw_test_accuracy), raw_test_accuracy, c = 'r', marker = 'o', alpha = .5, zorder=3)


    ### plot idtracker.ai network modifications accuracies (supplementary figure)
    for i, test_name in enumerate(new_ordered_test_names[1:]):
        this_test_info = tests_data_frame[tests_data_frame['test_name'] == test_name]
        CNN_model = int(this_test_info.CNN_model)
        label = cnn_model_names_dict[CNN_model]
        results_data_frame_test = results_data_frame.query('test_name == @test_name')
        repetition_averaged_data_frame = get_repetition_averaged_data_frame(results_data_frame_test)
        repetition_std_data_frame = get_repetition_std_data_frame(results_data_frame_test)

        ''' accuracy '''
        repetition_averaged_data_frame = repetition_averaged_data_frame.apply(pd.to_numeric, errors='ignore')
        accuracy = repetition_averaged_data_frame.test_accuracy.values
        std_accuracy = repetition_std_data_frame.test_accuracy.values
        group_sizes = repetition_averaged_data_frame.group_size.values.astype('float32')
        if CNN_model <= 5:
            marker = MARKERS[i+1]
            color = RGB_tuples(i/N)
            ax = ax_arr[0]
            (_, caps, _) = ax.errorbar(group_sizes, accuracy * 100, yerr = std_accuracy * 100, color = color, label = label, marker = marker, capsize = 5)
            for cap in caps:
                cap.set_markeredgewidth(1)

        if CNN_model > 5:
            marker = MARKERS[i+1]
            color = RGB_tuples(i/N)
            ax = ax_arr[1]
            (_, caps, _) = ax.errorbar(group_sizes, accuracy * 100, yerr = std_accuracy * 100, color = color, label = label, marker = marker, capsize = 5)
            for cap in caps:
                cap.set_markeredgewidth(1)

    convolutional_modifications_axes_settings(ax_arr, legend_order_conv, results_data_frame, repetition_averaged_data_frame)
    fc_modifications_axes_settings(ax_arr, legend_order_conv, results_data_frame, repetition_averaged_data_frame)
    main_fig_axes_settings(ax2,repetition_averaged_data_frame)

    plt.minorticks_off()

    plt.show()
    fig1.savefig(os.path.join(os.path.dirname(os.path.dirname(path_to_results_data_frame)), 'Supplementary_Figure_2.pdf'), transparent=True)
    fig2.savefig(os.path.join(os.path.dirname(os.path.dirname(path_to_results_data_frame)), 'Figure_1_f.pdf'), transparent=True)
