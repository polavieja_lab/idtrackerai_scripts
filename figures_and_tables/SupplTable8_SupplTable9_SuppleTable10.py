from __future__ import absolute_import, division, print_function
import os
import pandas as pd
from idtrackerai.utils.GUI_utils import selectDir


def f_string(x):
    return x


def f_percentage(x):
    return '%.2f' % (x*100) if x != 1 else '%i' % (x*100)


def f_accuracy(x):
    return '%.3f' % (x*100) if x != 1 else '%i' % (x*100)


def f_individual_accuracy(x):
    return '%.3f' % (x*100) if x != 1 else '%i' % (x*100)


def f_time(x):
    return '%.2f' % x


def f_area(x):
    return '%i' % x


def f_float(x):
    return '%.2f' % x


def f_integer(x):
    return '%i' % x


def f_boolean(x):
    return 'yes' if x else 'no'


def write_latex_table_for_subset_dataframe(save_folder, data_frame,
                                           columns_to_include,
                                           new_columns_names, formatters,
                                           subset_condition, subtable_name):
    assert len(columns_to_include) == len(new_columns_names) == len(formatters)
    subset_data_frame = data_frame[subset_condition]
    subset_data_frame = subset_data_frame[columns_to_include].copy()
    subset_data_frame.columns = new_columns_names
    latex_table_name = subtable_name + '.tex'
    latex_table_path = os.path.join(save_folder, latex_table_name)
    with open(latex_table_path, 'w') as file:
        file.write(subset_data_frame.to_latex(index=False,
                                              formatters=formatters))


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description=
         """This script generates the results shown in the Supplementary Tables 8 9 and 10.
        The tables are generated from the file image_quality_data_frame.pkl file
        that can be found in the 'idtracker.ai_figures_and_tables_data/SupplFig7_8_SupplTab8_9_10data'
        folder. This file can be generated from the script 'build_dataframe_SupplTables_8_9_10.py'
        that is in the folder 'build_data_frames'. The raw data can be obtained from
        idtracker.ai or from the corresponding author of the paper upon request.
        The output latex tables are saved in the folder 'idtracker.ai_figures_and_tables_data'
        with the names SupplTab8.tex. SupplTab9.tex and SupplTab10.tex.""")
    parser.add_argument("-iqdf", "--image_quality_data_frame_path", type=str,
                        help="path to the image_quality_data_frame.pkl file")
    args = parser.parse_args()
    image_quality_data_frame = \
        pd.read_pickle(args.image_quality_data_frame_path)

    print("Gaussian blurring table")
    columns_to_include = ['number_of_animals',
                          'sigma_gaussian_blurring',
                          'tracking_time',
                          'protocol',
                          'accuracy']
    new_columns_names = ['Num of anim.',
                         'Sigma G. blur',
                         'Tracking time',
                         'Protocol',
                         'Accuracy']
    formatters = [f_integer,
                  f_float,
                  f_string,
                  f_integer,
                  f_accuracy]
    condition = [x or y for (x,y) in zip(list(image_quality_data_frame.image_quality_condition == 'gaussian_blurring'),
                                              list(image_quality_data_frame.image_quality_condition == 'raw_videos'))]
    save_folder = os.path.dirname(os.path.dirname(args.image_quality_data_frame_path))
    write_latex_table_for_subset_dataframe(save_folder,
                                           image_quality_data_frame,
                                           columns_to_include,
                                           new_columns_names,
                                           formatters, condition,
                                           'SupplTab9')

    print("Resolution reduction table")
    columns_to_include = ['number_of_animals',
                          'mean_area_in_pixels',
                          'effective_number_of_pixels',
                          'size',
                          'tracking_time',
                          'protocol',
                          'accuracy']
    new_columns_names = ['Num of anim.',
                         'Pixels per animal',
                         'Eff.pixels per animal',
                         'Size (Gb)',
                         'Tracking time',
                         'Protocol',
                         'Accuracy']
    formatters = [f_integer,
                  f_integer,
                  f_integer,
                  f_float,
                  f_string,
                  f_integer,
                  f_accuracy]
    condition = [x or y for (x,y) in zip(list(image_quality_data_frame.image_quality_condition == 'resolution_reduction'),
                                              list(image_quality_data_frame.image_quality_condition == 'raw_videos'))]
    write_latex_table_for_subset_dataframe(save_folder,
                                           image_quality_data_frame,
                                           columns_to_include,
                                           new_columns_names,
                                           formatters, condition,
                                           'SupplTab8')

    print("Compression and inhomogeneous light table")
    columns_to_include = ['number_of_animals',
                          'compression',
                          'size',
                          'tracking_time',
                          'protocol',
                          'accuracy']
    new_columns_names = ['Num of anim.',
                         'Compression',
                         'Size (Gb)',
                         'Tracking time',
                         'Protocol',
                         'Accuracy']
    formatters = [f_integer,
                  f_string,
                  f_float,
                  f_string,
                  f_integer,
                  f_accuracy]
    condition = [x or y for (x,y) in zip(list(image_quality_data_frame.image_quality_condition == 'compression'),
                                              list(image_quality_data_frame.image_quality_condition == 'raw_videos'))]
    write_latex_table_for_subset_dataframe(save_folder,
                                           image_quality_data_frame,
                                           columns_to_include,
                                           new_columns_names,
                                           formatters, condition,
                                           'SupplTab10')
