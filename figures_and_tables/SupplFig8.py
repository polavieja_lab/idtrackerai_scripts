from __future__ import print_function, absolute_import, division
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import seaborn as sns
plt.ion()


def get_data_image_quality_dataframe(image_quality_df):
    res_reduc_df = image_quality_df[
        (image_quality_df['image_quality_condition'] == 'gaussian_blurring') | (image_quality_df['image_quality_condition'] == 'raw_videos')]
    gaussian_blurring_per_video = []
    accuracy_per_video = []
    sample_images_per_video = []
    protocol_per_video = []

    for number_of_animals in np.unique(res_reduc_df.number_of_animals):
        df = res_reduc_df[res_reduc_df.number_of_animals == number_of_animals]
        gaussian_blurring_per_video.append(list(df.sigma_gaussian_blurring))
        accuracy_per_video.append(list(df.accuracy))
        sample_images_per_video.append(list(df.sample_image))
        protocol_per_video.append(list(df.protocol))

    return gaussian_blurring_per_video,\
        accuracy_per_video, sample_images_per_video,\
        protocol_per_video


def plot_resolution_reduction_summary(gaussian_blurring_per_video,
                                      accuracy_per_video,
                                      sample_images_per_video,
                                      protocol_per_video):

    fig, ax1 = plt.subplots(1, 1, figsize=(10, 6))
    ax1.set_xlabel('Sigma gaussian kernel', fontsize=16)
    ax1.set_ylabel('human validated accuracy', fontsize=16)
    plt.tick_params(axis='both', which='major', labelsize=14)
    colors = ['b', 'r']
    markers = ['o', 's']
    labels = ['8 adult zebrafish', '60 juvenile zebrafish']
    plot_handles = []
    for i, (gb, acc, pr) in enumerate(zip(gaussian_blurring_per_video,
                                              accuracy_per_video,
                                              protocol_per_video)):
        gb = np.asarray(gb)
        acc = np.asarray(acc)*100
        pr = np.asarray(pr)
        order = np.argsort(gb)
        gb = gb[order]
        acc = acc[order]
        pr = pr[order]
        h_plot, = ax1.plot(gb, acc, '-', c=colors[i], label=labels[i])
        plot_handles.append(h_plot)
        for protocol, accuracy, gs in zip(pr, acc, gb):
            marker = markers[0] if protocol == 2 else markers[1]
            ax1.scatter(gs, accuracy, marker=marker, c=colors[i])
            if colors[i]=='r':
                epsilon = 0.5
            else:
                epsilon = -0.5
            ax1.text(gs, accuracy+epsilon, '%.4f' % accuracy, color=colors[i], verticalalignment='center', horizontalalignment='center')
    legend1 = plt.legend(handles=plot_handles,
                         loc=(.05, .025),
                         title='Video',
                         frameon=True,
                         fontsize=14)

    protocol_2 = mlines.Line2D([], [], color='k', marker='o', markersize=6,
                               label='Protocol 2', markeredgecolor='k',
                               markeredgewidth=1, markerfacecolor='None',
                               linestyle='None')
    protocol_3 = mlines.Line2D([], [], color='k', marker='s', markersize=6,
                               label='Protocol 3', markeredgecolor='k',
                               markeredgewidth=1, markerfacecolor='None',
                               linestyle='None')

    plt.legend(handles=[protocol_2, protocol_3],
               loc=(.05, .25), title='Tracking protocol',
               frameon=True, fontsize=14)
    ax1.get_legend().get_title().set_fontsize('14')
    plt.gca().add_artist(legend1)
    legend1.get_title().set_fontsize('14')
    sample_images = sample_images_per_video[1]
    gaussian_blurring = gaussian_blurring_per_video[1]
    order = np.argsort(gaussian_blurring)
    for i, o in enumerate(order):
        a = plt.axes([.13+.1*i, .55, .15, .15])
        a.imshow(sample_images[o], interpolation='nearest', cmap='gray')
        a.set_xticks([]), a.set_yticks([])
    ax1.text(2.5, 91, 'Sample identification images', ha='center', fontsize=14)
    sns.despine(ax=ax1, right=True, top=True)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description=
         """This script generates the Supplementary Figure 8.
         The plots are generated from the 'image_quality_data_frame.pkl' file
         stored in the folder idtrackerai_figures_and_tables_data/SupplFig7_8_SupplTab8_9_10data'.
         This file can be reprodudced runing the script build_image_quality_data_frame.py.
         Please refer to that script for more information.""")
    parser.add_argument("-iqdf", "--image_quality_data_frame_path", type=str,
                        help="path to the image_quality_data_frame.pkl file")
    args = parser.parse_args()
    image_quality_data_frame = \
        pd.read_pickle(args.image_quality_data_frame_path)
    gaussian_blurring_per_video,\
        accuracy_per_video, sample_images_per_video,\
        protocol_per_video = \
        get_data_image_quality_dataframe(image_quality_data_frame)

    plot_resolution_reduction_summary(gaussian_blurring_per_video,
                                      accuracy_per_video,
                                      sample_images_per_video,
                                      protocol_per_video)
    plt.show()
    test_folder = os.path.split(args.image_quality_data_frame_path)[0]
    plt.gcf().savefig(os.path.join(os.path.dirname(test_folder), 'SupplFig_8.pdf'))
