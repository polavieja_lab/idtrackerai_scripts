from __future__ import absolute_import, division, print_function
import os
import pandas as pd


def f_string(x):
    return x


def f_percentage(x):
    return '%.2f' % (x*100) if x != 1 else '%i' % (x*100)


def f_accuracy(x):
    return '%.3f' % (x*100) if x != 1 else '%i' % (x*100)


def f_individual_accuracy(x):
    return '%.3f' % (x*100) if x != 1 else '%i' % (x*100)


def f_time(x):
    return '%.2f' % x


def f_area(x):
    return '%i' % x


def f_float(x):
    return '%.2f' % x


def f_integer(x):
    return '%i' % x


def f_boolean(x):
    return 'yes' if x else 'no'


def write_latex_table_for_subset_dataframe(tracked_videos_folder, data_frame,
                                           columns_to_include,
                                           new_columns_names, formatters,
                                           subset_condition, subtable_name):
    assert len(columns_to_include) == len(new_columns_names) == len(formatters)
    subset_data_frame = data_frame[subset_condition]
    subset_data_frame = subset_data_frame[columns_to_include].copy()
    subset_data_frame.columns = new_columns_names
    latex_table_name = subtable_name + '.tex'
    latex_table_path = os.path.join(tracked_videos_folder, latex_table_name)
    with open(latex_table_path, 'w') as file:
        file.write(subset_data_frame.to_latex(index=False,
                                              formatters=formatters))


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description=
        """This script generates the results shown in the Supplementary Table 11.
        The table is generated from the file transfer_learning_data_frame.pkl file
        that can be found in the 'idtracker.ai_figures_and_tables_data/SupplTab11_data'
        folder. This file can be generated from the script 'build_dataframe_SupplTable_11.py'
        that is in the folder 'build_data_frames'. The raw data can be obtained from
        idtracker.ai or from the corresponding author of the paper upon request.
        The output latex table is saved in the folder 'idtracker.ai_figures_and_tables_data'
        with the name SupplTab11.tex. """)
    parser.add_argument("-tldf", "--transfer_learning_data_frame_path", type=str,
                        help="path to the transfer_learning_data_frame.pkl file")
    args = parser.parse_args()
    transfer_learning_data_frame = \
        pd.read_pickle(args.transfer_learning_data_frame_path)

    print("Transfer learning table")
    columns_to_include = ['number_of_animals',
                          'knowledge_transfer',
                          'protocol_cascade_time',
                          'protocol',
                          'accuracy',
                          'accuracy_identified_animals',
                          'percentage_of_individual_blobs_identified']
    new_columns_names = ['Num of anim.',
                         'Transfer learning',
                         'Protocol cascade time',
                         'Protocol',
                         'Accuracy',
                         'Accuracy identified animals',
                         'Perc. animals identified']
    formatters = [f_integer,
                  f_boolean,
                  f_string,
                  f_integer,
                  f_accuracy,
                  f_accuracy,
                  f_accuracy]
    condition = [x for x in
                 list(transfer_learning_data_frame.number_of_animals > 0)]
    save_folder = os.path.dirname(os.path.dirname(args.transfer_learning_data_frame_path))
    write_latex_table_for_subset_dataframe(save_folder,
                                           transfer_learning_data_frame,
                                           columns_to_include,
                                           new_columns_names,
                                           formatters, condition,
                                           'SupplTab11')
