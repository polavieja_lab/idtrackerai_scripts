from __future__ import absolute_import, division, print_function
import os
import pandas as pd
import time


def f_string(x):
    return x


def f_percentage(x):
    return '%.2f' % (x*100) if x != 1 else '%i' % (x*100)


def f_accuracy(x):
    return '%.3f' % (x*100) if x != 1 else '%i' % (x*100)


def f_individual_accuracy(x):
    return '%.3f' % (x*100) if x != 1 else '%i' % (x*100)


def f_time(x):
    return time.strftime("%H:%M:%S", time.gmtime(x))


def f_area(x):
    return '%i' % x


def f_float(x):
    return '%.2f' % x


def f_integer(x):
    return '%i' % x


def f_boolean(x):
    return 'yes' if x else 'no'

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description=
        """This script generates the results shown in the Supplementary Tables 5 6 and 7.
       The tables are generated from the file tracked_videos_data_frame.pkl file
       that can be found in the 'idtracker.ai_figures_and_tables_data/tracked_videos'
       folder. This file can be generated from the script 'build_tracked_videos_data_frame.py'
       that is in the folder 'build_data_frames'. The raw data can be obtained from
       idtracker.ai or from the corresponding author of the paper upon request.
       The output latex tables are saved in the folder 'idtracker.ai_figures_and_tables_data'
       with the names SupplTab5.tex. SupplTab6.tex and SupplTab7.tex.""")
    parser.add_argument("-tvdf", "--tracked_videos_data_frame", type=str,
                        help="path to the tracked_videos_data_frame")
    args = parser.parse_args()
    tracked_videos_data_frame = \
        pd.read_pickle(args.tracked_videos_data_frame)
    save_folder = os.path.dirname(os.path.dirname(args.tracked_videos_data_frame))

    columns_to_include = ['video_title',
                        'video_length_sec',
                        'frame_rate',
                        'mean_area_in_pixels',
                        'protocol_used',
                        'number_of_crossing_fragments_in_validated_part',
                        'accuracy_in_accumulation',
                        'accuracy_identification_and_interpolation',
                        'individual_accurcay_identified_animals_interpolated',
                        'rate_nonidentified_animals_indentification_and_interpolation',
                        'rate_misidentified_animals_identification_and_interpolation']

    new_columns_names = ['Video',
                'Duration',
                'Frame rate (fps)',
                'Pixels per animal',
                'Protocol',
                'Number of crossings',
                'Accuracy in accumulation',
                'Accuracy',
                'Individual Accuracy',
                'perc. not identified',
                'perc. misidentified']

    formatters = [f_string,
                    f_time,
                    f_integer,
                    f_area,
                    f_integer,
                    f_integer,
                    f_accuracy,
                    f_accuracy,
                    f_individual_accuracy,
                    f_accuracy,
                    f_accuracy]

    def write_latex_table_for_subset_dataframe(save_folder, data_frame, columns_to_include, new_columns_names, formatters, subset_condition, subtable_name):
        assert len(columns_to_include) == len(new_columns_names) == len(formatters)

        subset_data_frame = data_frame[subset_condition]
        subset_data_frame = subset_data_frame[columns_to_include].copy()
        subset_data_frame.columns = new_columns_names
        latex_table_name = subtable_name + '.tex'
        latex_table_path = os.path.join(save_folder, latex_table_name)
        with open(latex_table_path,'w') as file:
            file.write(subset_data_frame.to_latex(index = False, formatters = formatters))

    ### smaller groups videos
    print("generating smaller groups table")
    condition = [x and not y for (x,y) in zip(list(tracked_videos_data_frame.number_of_animals < 35), list(tracked_videos_data_frame.bad_example))]
    # condition = tracked_videos_data_frame.number_of_animals <= 35 and not tracked_videos_data_frame.idTracker_video
    write_latex_table_for_subset_dataframe(save_folder, tracked_videos_data_frame, columns_to_include, new_columns_names, formatters, condition, 'SupplTab5')
    ### larger groups videos
    print("generating larger groups videos table")
    condition = [x and not y for (x,y) in zip(list(tracked_videos_data_frame.number_of_animals >= 35), list(tracked_videos_data_frame.bad_example))]
    write_latex_table_for_subset_dataframe(save_folder, tracked_videos_data_frame, columns_to_include, new_columns_names, formatters, condition, 'SupplTab6')
    ### idTracker videos
    print("generating bad videos table")
    condition = [bool(x) for x in tracked_videos_data_frame.bad_example]
    write_latex_table_for_subset_dataframe(save_folder, tracked_videos_data_frame, columns_to_include, new_columns_names, formatters, condition, 'SupplTab7')
