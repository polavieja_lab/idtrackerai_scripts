from __future__ import absolute_import, division, print_function
import numpy as np
from idtrackerai.blob import Blob
from scipy.stats import gamma


class BlobsListConfig(object):
    def __init__(self, number_of_animals=None, scale_parameter=None,
                 shape_parameter=None, number_of_frames=None,
                 repetition=None, starting_frame=None):
        self.number_of_animals = number_of_animals
        self.scale_parameter = scale_parameter
        self.shape_parameter = shape_parameter
        self.max_number_of_frames_per_fragment = number_of_frames
        self.min_number_of_frames_per_fragment = 1
        self.number_of_frames = number_of_frames
        self.repetition = repetition
        self.starting_frame = starting_frame
        self.IMDB_codes = []
        self.ids_codes = []


def subsample_dataset_by_individuals(dataset, config):
    # We need to consider that for every individual fragment there are two frames that are not considered for training
    # In order to have the correct number of trainable images per individual fragment we need to increase the number of
    # frames in the video to account for those two frames. The total number of frames needes is:
    # number_of_frames = int(config.number_of_frames + 2 * config.number_of_frames / config.number_of_frames_per_fragment)
    number_of_frames = config.number_of_frames
    if config.number_of_animals > dataset.number_of_animals:
        raise ValueError("The number of animals for subsampling (%i) cannot be bigger than the number of animals in the dataset (%i)" %(config.number_of_animals, dataset.number_of_animals))

    if number_of_frames > dataset.minimum_number_of_images_per_animal:
        raise ValueError("The number of frames for subsampling (%i) cannot be bigger than the minimum number of images per animal in the dataset (%i)" %(number_of_frames, dataset.minimum_number_of_images_per_animal))

    # copy dataset specifics to config. This allows to restore the dataset if needed
    config.IMDB_codes = dataset.IMDB_codes
    config.ids_codes = dataset.ids_codes
    # set permutation of individuals
    np.random.seed(config.repetition)
    # old way os sampling individuals
    # config.identities = dataset.identities[np.random.permutation(config.number_of_animals)]
    permutation_individual_indices = np.random.permutation(dataset.number_of_animals)
    config.identities = permutation_individual_indices[:config.number_of_animals]
    print("identities, ", config.identities)
    # set stating frame
    # we set the starting frame so that we take images from both videos of the library.
    if config.starting_frame is None:
        if dataset.minimum_number_of_images_per_animal > 20000:
            # This is the code used when the part A and B of CARP were together
            config.starting_frame = np.random.randint(int(dataset.minimum_number_of_images_per_animal/3)-number_of_frames)
        else:
            config.starting_frame = np.random.randint(int(dataset.minimum_number_of_images_per_animal)-number_of_frames)

    print("starting frame, ", config.starting_frame)

    subsampled_images = []
    subsampled_centroids = []
    for identity in config.identities:
        indices_identity = np.where(dataset.labels == identity)[0]
        subsampled_images.append(np.expand_dims(dataset.images[indices_identity][config.starting_frame:config.starting_frame + number_of_frames], axis = 1))
        subsampled_centroids.append(np.expand_dims(dataset.centroids[indices_identity][config.starting_frame:config.starting_frame + number_of_frames], axis = 1))

    return np.concatenate(subsampled_images, axis = 1), np.concatenate(subsampled_centroids, axis = 1)


def get_next_number_of_blobs_in_fragment(config):
    scale = config.scale_parameter
    shape = config.shape_parameter
    X = gamma(a=shape, loc=1, scale=scale)
    number_of_frames_per_fragment = int(X.rvs(1))
    while number_of_frames_per_fragment < config.min_number_of_frames_per_fragment or number_of_frames_per_fragment > config.max_number_of_frames_per_fragment:
        number_of_frames_per_fragment = int(X.rvs(1))

    return number_of_frames_per_fragment


def generate_list_of_blobs(identification_images, centroids, config):

    blobs_in_video = []
    number_of_fragments = 0

    print("\n***********Generating list of blobs")
    print("centroids shape ", centroids.shape)
    print("identification_images shape", identification_images.shape)
    for identity in range(config.number_of_animals):
        # decide length of first individual fragment for this identity
        number_of_blobs_per_fragment = \
            get_next_number_of_blobs_in_fragment(config)
        number_of_fragments += 1
        number_of_frames_per_crossing_fragment = 3
        blobs_in_fragment = 0
        blobs_in_crossing_fragment = 0
        blobs_in_identity = []
        for frame_number in range(config.number_of_frames):
            centroid = centroids[frame_number,identity,:]
            image = identification_images[frame_number,identity,:,:]

            blob = Blob(centroid, None, None, None,
                        number_of_animals = config.number_of_animals)
            blob.frame_number = frame_number

            if blobs_in_fragment <= number_of_blobs_per_fragment:
                blob._is_an_individual = True
                blob._is_a_crossing = False
                blob._image_for_identification = \
                    ((image - np.mean(image))/np.std(image)).astype("float32")
                blob._user_generated_identity = identity + 1
                if blobs_in_fragment != 0:
                    blob.previous = [blobs_in_identity[frame_number-1]]
                    blobs_in_identity[frame_number-1].next = [blob]
                blobs_in_fragment += 1
            elif blobs_in_crossing_fragment < number_of_frames_per_crossing_fragment:
                blobs_in_crossing_fragment += 1
                blob = None
            else:
                blob._is_an_individual = True
                blob._is_a_crossing = False
                blob._image_for_identification = \
                    ((image - np.mean(image))/np.std(image)).astype("float32")
                blob._user_generated_identity = identity + 1
                blob.previous = []

                blobs_in_fragment = 1
                blobs_in_crossing_fragment = 0
                number_of_fragments += 1
                number_of_blobs_per_fragment = \
                    get_next_number_of_blobs_in_fragment(config)
            blobs_in_identity.append(blob)
        blobs_in_video.append(blobs_in_identity)

    blobs_in_video = zip(*blobs_in_video)
    blobs_in_video = [[b for b in blobs_in_frame if b is not None] for blobs_in_frame in blobs_in_video]
    print("number of fragments ", number_of_fragments)
    return blobs_in_video
