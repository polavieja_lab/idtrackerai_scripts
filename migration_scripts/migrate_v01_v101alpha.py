from __future__ import absolute_import, division, print_function
import os
import sys

import numpy as np
import glob
from pprint import pprint
from idtrackerai import video
from idtrackerai.preprocessing import model_area
from idtrackerai import blob
from idtrackerai import list_of_blobs
from idtrackerai import fragment
from idtrackerai import list_of_fragments
from idtrackerai import globalfragment
from idtrackerai import list_of_global_fragments
from idtrackerai.groundtruth_utils import generate_groundtruth
from idtrackerai.groundtruth_utils import generate_individual_groundtruth

# sys.setrecursionlimit(100000)
sys.path.append('/home/themis/idtrackerai/idtrackerai')
sys.path.append('/home/themis/idtrackerai/idtrackerai/preprocessing')
sys.path.append('/home/themis/idtrackerai/idtrackerai/groundtruth_utils')


def migrate_video(video_object):
    print('Migrating video object...')
    nv = video.Video()
    nv.__dict__.update(video_object.__dict__)
    nv._model_area = model_area.ModelArea(video_object.model_area.mean,
                                          video_object.model_area.median,
                                          video_object.model_area.std)
    nv.model_area.__dict__.update(video_object.model_area.__dict__)
    return nv


def migrate_list_of_blobs(lb, nv, path_to_save):
    def migrate_blob(b):
        nb = blob.Blob(b.centroid,
                       b.contour,
                       b.area,
                       b.bounding_box_in_frame_coordinates)
        nb.__dict__.update(b.__dict__)
        return nb

    def migrate_blobs_in_video(bv):
        nbv = []
        for bf in bv:
            nbf = []
            for b in bf:
                nbf.append(migrate_blob(b))
            nbv.append(nbf)
        return nbv

    print('Migrating list of blobs... ', os.path.split(path_to_save)[-1])
    nbv = migrate_blobs_in_video(lb.blobs_in_video)
    nlb = list_of_blobs.ListOfBlobs(blobs_in_video=nbv)
    nlb.__dict__.update(lb.__dict__)
    nlb.blobs_in_video = nbv
    nlb.save(nv, path_to_save=path_to_save)
    return nlb


def migrate_list_of_fragments(lf, path_to_save):
    def migrate_fragment(f):
        nf = fragment.Fragment(centroids=f.centroids,
                               number_of_animals=f.number_of_animals)
        nf.__dict__.update(f.__dict__)
        return nf

    def migrate_fragments(fs):
        nfs = []
        for f in fs:
            nf = migrate_fragment(f)
            nfs.append(nf)
        return nfs
    print('Migrating list of fragments...')
    nfs = migrate_fragments(lf.fragments)
    nlf = list_of_fragments.ListOfFragments(fragments=nfs)
    nlf.__dict__.update(lf.__dict__)
    nlf.fragments = nfs
    nlf.save(path_to_save)
    [setattr(f, 'coexisting_individual_fragments', None)
        for f in nlf.fragments]
    return nlf


def migrate_list_of_global_fragments(lgf, nbv, nfs, path_to_save):
    def migrate_global_fragment(gf, nbv, nfs):
        ngf = globalfragment.GlobalFragment(nbv, nfs,
                                            gf.index_beginning_of_fragment,
                                            gf.number_of_animals)
        ngf.__dict__.update(gf.__dict__)
        return ngf

    def migrate_global_fragments(gfs, nbv, nfs):
        ngfs = []
        for gf in gfs:
            ngf = migrate_global_fragment(gf, nbv, nfs)
            ngfs.append(ngf)
        return ngfs
    print('Migrating list of global fragments...')
    ngf = migrate_global_fragments(lgf.global_fragments, nbv, nfs)
    nnagf = migrate_global_fragments(lgf.non_accumulable_global_fragments,
                                     nbv, nfs)
    fgffa = migrate_global_fragment(lgf.first_global_fragment_for_accumulation,
                                    nbv, nfs)

    nlgf = list_of_global_fragments.ListOfGlobalFragments(ngf)
    nlgf.__dict__.update(lgf.__dict__)
    nlgf.global_fragments = ngf
    nlgf.non_accumulable_global_fragments = nnagf
    nlgf.first_global_fragment_for_accumulation = fgffa
    nlgf.save(path_to_save, nfs)
    return nlgf


def migrate_session_folder(session_path):
    print("Session to be migrated: ", session_path)
    video_path = os.path.join(session_path, 'video_object.npy')
    print("Video path: ", video_path)
    video_object = np.load(video_path).item()
    video_object.update_paths(video_path)
    nv = migrate_video(video_object)
    nv.save()
    print('loading list of blobs segmented...')
    lbs = np.load(video_object.blobs_path_segmented).item()
    migrate_list_of_blobs(lbs, nv, nv.blobs_path_segmented)
    del lbs
    print('loading list of blobs...')
    lb = np.load(video_object.blobs_path).item()
    nlb = migrate_list_of_blobs(lb, nv, nv.blobs_path)
    del lb
    print('loading list of blobs interpolated...')
    lbi = np.load(video_object.blobs_path_interpolated).item()
    migrate_list_of_blobs(lbi, nv, nv.blobs_path_interpolated)
    del lbi
    print('loading list of blobs no gaps...')
    lbng = np.load(video_object.blobs_no_gaps_path).item()
    migrate_list_of_blobs(lbng, nv, nv.blobs_no_gaps_path)
    del lbng
    print('loading list of fragments')
    lf = list_of_fragments.ListOfFragments.load(video_object.fragments_path)
    nlf = migrate_list_of_fragments(lf, video_object.fragments_path)
    del lf
    print('loading list of global fragments')
    lgf = list_of_global_fragments.ListOfGlobalFragments.load(
        nv.global_fragments_path, nlf.fragments)
    migrate_list_of_global_fragments(lgf, nlb.blobs_in_video,
                                     nlf.fragments,
                                     nv.global_fragments_path)
    del lgf
    del nv, nlb, nlf

def migrate_groundtruth(video_folder):
    def migrate_groundtruth_blob(gtb):
        ngtb = generate_groundtruth.GroundTruthBlob()
        ngtb.__dict__.update(gtb.__dict__)
        return ngtb

    def migrate_groundtruth_blobs_in_video(gtbv):
        ngtbv = []
        for gtbf in gtbv:
            ngtbf = []
            for gtb in gtbf:
                ngtbf.append(migrate_groundtruth_blob(gtb))
            ngtbv.append(ngtbf)
        return ngtbv
    print('Migrating grountruth file...')
    groundtruth_path = os.path.join(video_folder, '_groundtruth.npy')
    if os.path.isfile(groundtruth_path):
        print(groundtruth_path, 'exsits ')
        gt = np.load(groundtruth_path).item()
        ngt = generate_groundtruth.GroundTruth()
        ngt.__dict__.update(gt.__dict__)
        ngt.blobs_in_video = migrate_groundtruth_blobs_in_video(
            ngt.blobs_in_video)
        ngt.video = migrate_video(ngt.video)
        np.save(groundtruth_path, ngt)
        return ngt, gt
    else:
        print(groundtruth_path, 'does not exsits ')


def migrate_individual_groundtruth_files(video_folder):
    def migrate_individual_groundtruth_blob(igtb):
        nigtb = generate_individual_groundtruth.GroundTruthBlob()
        nigtb.__dict__.update(igtb.__dict__)
        return nigtb

    def migrate_individual_groundtruth_blobs_in_video(igtbv):
        nigtbv = []
        for igtb in igtbv:
            nigtbv.append(migrate_individual_groundtruth_blob(igtb))
        return nigtbv
    print('Migrating individual groundtruth files...')
    igt_files = [file for file in glob.glob(video_folder + '/*')
                 if 'individual' in file]
    pprint(igt_files)
    if len(igt_files) != 0:
        for igt_path in igt_files:
            print("migrating ", igt_path)
            igt = np.load(igt_path).item()
            nigt = generate_individual_groundtruth.IndividualGroundTruth()
            nigt.__dict__.update(igt.__dict__)
            nigt.individual_blobs_in_video = \
                migrate_individual_groundtruth_blobs_in_video(
                    nigt.individual_blobs_in_video)
            nigt.video = migrate_video(nigt.video)
            np.save(igt_path, nigt)
        return nigt, igt
    else:
        print("There are no individual groundtruth files")


if __name__ == '__main__':
    try:
        from idtrackerai.utils.GUI_utils import selectDir
        session_path = selectDir('./')
    except Exception:
        session_path = raw_input('insert the path to the session to migrate')
    migrate_session_folder(session_path)
    video_folder = os.path.split(session_path)[0]
    migrate_groundtruth(video_folder)
    migrate_individual_groundtruth_files(video_folder)
