from __future__ import absolute_import, division, print_function
import os
import sys
from pprint import pprint

from migrate_v01_v101alpha import migrate_session_folder
from migrate_v01_v101alpha import migrate_groundtruth
from migrate_v01_v101alpha import migrate_individual_groundtruth_files
sys.path.append('./plots')
from build_tracked_videos_data_frame import sessions


if __name__ == '__main__':
    try:
        from idtrackerai.utils.GUI_utils import selectDir
        path_to_results_hard_drive = \
            selectDir('/media', text="Select the hard drive with the name \
                      'ground_truth_results'")  # select path to video
    except KeyError:
        path_to_results_hard_drive = raw_input("Select the hard drive with\
                                               'groundtruth_results': ")
    tracked_videos_folder = os.path.join(path_to_results_hard_drive,
                                         'tracked_videos')
    session_paths = [x[0] for x in os.walk(tracked_videos_folder)
                     if 'session' in x[0][-16:] and 'Trash' not in x[0]]
    pprint(session_paths)
    sessions_that_failed = []
    if len(session_paths) == len(sessions):

        for session_path in session_paths:
            print("\n******************************")
            print('Session: ', session_path)
            try:
                migrate_session_folder(session_path)
                video_folder = os.path.split(session_path)[0]
                migrate_groundtruth(video_folder)
                migrate_individual_groundtruth_files(video_folder)
            except Exception as e:
                sessions_that_failed.append(session_path)
                print(e)

        pprint(sessions_that_failed)
