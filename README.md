# idtracker.ai scripts

This repository includes the scripts and instructions required to reproduce the
Figures, Supplementary Figures and Supplementary Tables in the paper:

[Romero-Ferrero, F., Bergomi, M.G., Hinz, R.C., Heras, F.J.H., de Polavieja, G.G., Nature Methods, 2019.
idtracker.ai: tracking all individuals in small or large collectives of unmarked animals.](http://dx.doi.org/10.1038/s41592-018-0295-5) (F.R.-F. and M.G.B. contributed equally to this work. Correspondence should be addressed to G.G.d.P: gonzalo.polavieja@neuro.fchampalimaud.org)

## Requirements

This repository requires the following packages:

* [idtracker.ai](https://gitlab.com/polavieja_lab/idtrackerai)
* [CARP_library](https://gitlab.com/polavieja_lab/CARP_library)
* [netowkrx](https://networkx.github.io/)

## Instructions

The scripts in the folder *figures_and_tables* reproduce the Figures, Supplementary Figures and Supplementary Table with the same name.

The information to run every script can be found in the argument *description* of the function *argparse.ArgumentParser()*. The instructions can also accessed running the following command:

    python script.py -h

This command can be run inside of the conda environment generated after properly installing [idtracker.ai](https://gitlab.com/polavieja_lab/idtrackerai).

### Notes about the computation of the accuracy indices:

The accuracy indices shown in Supplementary Tables 5, 6 and 7 were computed directly with the Global Validation and Individual Validation tabs in the
[idtracker.ai](idtracker.ai) GUI and the code can be found in the [idtracker.ai gitlab repository](https://gitlab.com/polavieja_lab/idtrackerai).
The accuracy indices shown in Supplementary Tables 4, 8, 9, 10 and 11 were computed using the script *compute_groundtruth_statistics_general.py* that
can be found in the *groundtruth_utils* both in this repository and in the [idtracker.ai gitlab repository](https://gitlab.com/polavieja_lab/idtrackerai).

### Notes abut the simulations with the library of individual images

The results from the Fig 1f, Supplementary Figure 2 and Supplementary Figure 6a are not generated from the tracking software idtracker.ai but from simulations of
parts of the software. This simulations can be found in the folder 'simulations'. The data needed to run the simulations can be found in [idtracker.ai](idtracker.ai).
The package [CARP_library](https://gitlab.com/polavieja_lab/CARP_library) needs to be installed to run these simulations.

### Notes about the Figure 1g-j

To generate this figures it is necessary to run the script *Fig1g_1h_1i_1j.py* in a computer with at least 64Gb of RAM memory. This
is because the script loads multiple objects generated from idtracker.ai that contain a lot of data.

### Note about the Supplementary Figure 10

This suplementary figure is the only one generated from Matlab script.
This figure needs is not saved automatically and needs to be saved after the scripts generates it.
