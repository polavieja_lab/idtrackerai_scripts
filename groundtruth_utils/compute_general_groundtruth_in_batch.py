import os
import sys
import glob
from pprint import pprint
import numpy as np
from compute_groundtruth_statistics_general import compute_and_save_session_accuracy_wrt_groundtruth

if sys.argv[0] == 'idtrackeraiApp.py' or 'idtrackeraiGUI' in sys.argv[0]:
    from kivy.logger import Logger
    logger = Logger
else:
    import logging
    logger = logging.getLogger("__main__.compute_statistics_against_groundtruth")


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-vf", "--video_folder", type=str,
                        help="folder where all the session folder for a given\
                        video are")
    parser.add_argument("-gt", "--groundtruth_type", type=str,
                        help="type of groundtruth to compute \
                        ('no_gaps' or 'normal')")
    args = parser.parse_args()

    session_folders = [folder for folder in glob.glob(os.path.join(args.video_folder,"*")) if "session" in folder]

    pprint(session_folders)

    for session_folder in session_folders:
        print("\n")
        groundtruth_type = args.groundtruth_type
        video_object_path = os.path.join(session_folder, 'video_object.npy')
        logger.info("loading video object...")
        video = np.load(video_object_path).item(0)
        video.update_paths(video_object_path)
        groundtruth_path = os.path.join(video.video_folder, '_groundtruth.npy')
        groundtruth = np.load(groundtruth_path).item()
        video, groundtruth = compute_and_save_session_accuracy_wrt_groundtruth(video, groundtruth_type)
        pprint(video.gt_accuracy)
